extends Node

const DateTime = preload("res://Utilidades/DateTime.gd")

var rawtime = 0

func _ready() -> void:
	GameManager.connect("empezar_nueva_partida", self, "_on_empezo_partida")	
	GameManager.connect("partida_cargada", self,"_on_empezo_partida")

func _on_empezo_partida() -> void:
	rawtime = 0
	get_tiempo_partida()

func _process(delta: float) -> void:
	rawtime += delta

func get_tiempo() -> String:
	var datetime = DateTime.new(rawtime)
	return datetime.get_text()

func get_tiempo_crudo() -> float:
	return rawtime

func get_tiempo_partida() -> void:
	var tiempo_partida = PartidasManager.get_tiempo_partida(GameManager.partida_id)
	rawtime += tiempo_partida



