extends Node

export(AudioStream) var audio_stream_moverse
export(AudioStream) var audio_stream_presionar

onready var nodo_audio: = get_node("Audio")

func _input(evento: InputEvent) -> void:
	if not GameDefinitions.on_gui: return
	if evento.is_action_pressed("ui_accept"):
		nodo_audio.stream = audio_stream_presionar
		nodo_audio.play()
	elif _moverse_por_interfaz(evento):
		nodo_audio.stream = audio_stream_moverse
		nodo_audio.play()

func _moverse_por_interfaz(e:InputEvent) -> bool:
	return e.is_action_pressed("ui_right") or e.is_action_pressed("ui_left") \
			or e.is_action_pressed("ui_down") or e.is_action_pressed("ui_up")
