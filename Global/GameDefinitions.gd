extends Node

############################ Variables del estado del Juego ###################

var on_gui = false


########################### ACCCIONES Y COMANDOS ##############################

const Comando_atacar:String = "atacar"
const Comando_pasar:String = "pasar"
const Comando_habilidad:String = "habilidad"
const Comando_magia:String = "magia"
const Comando_usar_objeto:String = "usar_objeto"

const Accion_atacar:String = "atacar"
const Acciones_magia = ["Fulgur","Aqua","Ignis","Sana"] #MagicDefinitions keys
const Acciones_articulo = ["1","2","3","4","5","6","7","8"] #Articulos ids
const Accion_habilidad = "Habilidad"
const Accion_pasar = "pasar"


#################### LIMITES ATRIBUTOS ########################################

const Vida_limite = 9999
const Atributos_limite = 999
const Tiempo_espera_minimo = 0.5
const Ultimo_nivel = 99

############################# Personajes/Jugadores ############################

enum TipoDeActor {ENEMIGO, JUGADOR}
enum PERSONAJES_IDs {CABALLERO = 0, GUERRERO = 1, MAGA_BLANCA = 2, MAGA_OSCURA = 3, NINJA = 4, ENEMIGO = -1}

const Caballero_escena: String = "res://sistema_de_combate/Actores/Jugador/Caballero_sagrado/CaballeroSagrado.tscn"
const Ninja_escena:String = "res://sistema_de_combate/Actores/Jugador/Ninja/Ninja.tscn"
const Guerrero_escena:String = "res://sistema_de_combate/Actores/Jugador/Guerrero/Guerrero.tscn"
const Maga_blanca_escena:String = "res://sistema_de_combate/Actores/Jugador/MagaBlanca/MagaBlanca.tscn"
const Maga_oscura_escena:String = "res://sistema_de_combate/Actores/Jugador/MagaOscura/MagaOscura.tscn"

const Personajes_ID = {
    Caballero_escena: PERSONAJES_IDs.CABALLERO,
    Ninja_escena: PERSONAJES_IDs.NINJA,
    Guerrero_escena: PERSONAJES_IDs.GUERRERO,
    Maga_blanca_escena: PERSONAJES_IDs.MAGA_BLANCA,
    Maga_oscura_escena: PERSONAJES_IDs.MAGA_OSCURA
}

const Personajes_nombres_by_escena = {
    Caballero_escena: "Caballero",
    Ninja_escena: "Ninja",
    Guerrero_escena: "Guerrero",
    Maga_blanca_escena: "Maga blanca",
    Maga_oscura_escena: "Maga oscura"
}

const Personajes_iconos_by_escena = {
    Caballero_escena: "res://recursos/personajes/CaballeroSagrado/icono.png",
    Ninja_escena: "res://recursos/personajes/NinjaShizuka/Ninja_Shizuka.png",
    Guerrero_escena: "res://recursos/personajes/ArtistaMarcial/icono.png",
    Maga_blanca_escena: "res://recursos/personajes/MagaBlanca/Icon.png",
    Maga_oscura_escena: "res://recursos/personajes/MagaOscura/Icon.png"
}

const Personajes_iconos_by_id = {
    PERSONAJES_IDs.CABALLERO: "res://recursos/personajes/CaballeroSagrado/icono.png",
    PERSONAJES_IDs.NINJA: "res://recursos/personajes/NinjaShizuka/Ninja_Shizuka.png",
    PERSONAJES_IDs.GUERRERO: "res://recursos/personajes/ArtistaMarcial/icono.png",
    PERSONAJES_IDs.MAGA_BLANCA: "res://recursos/personajes/MagaBlanca/Icon.png",
    PERSONAJES_IDs.MAGA_OSCURA: "res://recursos/personajes/MagaOscura/Icon.png"
}

const lista_jugadores = [Caballero_escena,Ninja_escena,Guerrero_escena,Maga_blanca_escena,Maga_oscura_escena]


################################# Enemigos #####################################

const Asesino_escena: String = "res://sistema_de_combate/Actores/Enemigos/Asesino/Asesino.tscn"
const Fantasma_escena: String = "res://sistema_de_combate/Actores/Enemigos/Fantasma/Fantasma.tscn"
const Gargola_escena: String = "res://sistema_de_combate/Actores/Enemigos/Gargola/Gargola.tscn"
const Goblin_escena: String = "res://sistema_de_combate/Actores/Enemigos/Goblin/Goblin.tscn"
const Ladron_escena: String = "res://sistema_de_combate/Actores/Enemigos/Ladron/Ladron.tscn"
const Minotauro_escena: String = "res://sistema_de_combate/Actores/Enemigos/Minotauro/Minotauro.tscn"
const Murcielago_escena: String = "res://sistema_de_combate/Actores/Enemigos/MurcielagoDemoniaco/MurcielagoDemoniaco.tscn"
const Quimera_escena: String = "res://sistema_de_combate/Actores/Enemigos/Quimera/Quimera.tscn"
const Rey_Demonio_escena : String = "res://sistema_de_combate/Actores/Enemigos/ReyDemonio/ReyDemonio.tscn"
const Skeleton_escena: String = "res://sistema_de_combate/Actores/Enemigos/Skeleton/Skeleton.tscn"
const Slime_escena: String = "res://sistema_de_combate/Actores/Enemigos/SlimeAzul/Slime.tscn"

############################# Niveles #########################################

const capital_key = "Capital"
const praderas_key = "Praderas"
const castillo_demoniaco = "Castillo Demoniaco"
const torre_final = "Torre Del Castillo Demoniaco"

const enemigos_capital = [
    Asesino_escena,
    Ladron_escena,
    Fantasma_escena
]
const enemigos_praderas = [
    Goblin_escena, 
    Minotauro_escena, 
    Asesino_escena,
    Slime_escena,
    Fantasma_escena,
    Ladron_escena,
    Minotauro_escena, 
    Fantasma_escena,
    Goblin_escena, 
    Fantasma_escena,
    Slime_escena,
    Slime_escena,
    Slime_escena,
    Slime_escena
]
const enemigos_castillo_demoniaco = [
    Skeleton_escena,
    Gargola_escena, 
    Goblin_escena, 
    Murcielago_escena,
    Quimera_escena, 
    Skeleton_escena,
    Gargola_escena, 
    Murcielago_escena,
    Quimera_escena, 
    Goblin_escena, 
    Minotauro_escena, 
    Minotauro_escena, 
    Fantasma_escena,
    Goblin_escena, 
]

const niveles = {
    capital_key: {
        "enemigos": enemigos_capital,
    },
    praderas_key: {
        "enemigos": enemigos_praderas
    },
    castillo_demoniaco: {
        "enemigos": enemigos_castillo_demoniaco
    },
    torre_final:{
        "enemigos": [Rey_Demonio_escena]
    }
}

const sub_niveles_info_by_id = {
    -1:{
        "nombre": "---",
        "nivel": "---"
    },
    0: {
        "nombre": "Distrito comercial",
        "nivel": "Capital"
    },
    1: {
        "nombre": "Distrito rojo",
        "nivel": "Capital"
    },
    2: {
        "nombre": "Distrito verde",
        "nivel": "Capital"
    },
    3:{
        "nombre": "Praderas",
        "nivel": "Praderas"
    },
    4: {
        "nombre": "Primer piso",
        "nivel": "Castillo Demoniaco"
    },
    5: {
        "nombre": "Piso Infernal",
        "nivel": "Castillo Demoniaco"
    }
}

###############################Variables de Armas#####################################
enum ARMAS_MATERIALES { HIERRO,DIAMANTE,MADERA,OBSIDIANA,DEMONIUM }

const Arma_icono_key = "icono"
const Arma_nombre_key = "nombre"
const Precio_key = "precio"
const arma_por_jugador = {
    PERSONAJES_IDs.CABALLERO: "Espada",
    PERSONAJES_IDs.GUERRERO: "Hacha",
    PERSONAJES_IDs.MAGA_BLANCA: "Baston",
    PERSONAJES_IDs.MAGA_OSCURA: "Vara",
    PERSONAJES_IDs.NINJA: "Daga"
}

const Armas = {
    PERSONAJES_IDs.CABALLERO: {
        ARMAS_MATERIALES.HIERRO: {
            Arma_nombre_key : "Espada de Hierro",
            Arma_icono_key: "res://recursos/armas/Espadas/espadaDeHierro.png",
            Precio_key: 500
        },
        ARMAS_MATERIALES.DIAMANTE: {
            Arma_nombre_key: "Espada Divina",
            Arma_icono_key: "res://recursos/armas/Espadas/espadaDeDiamante.png",
            Precio_key: 750
        },
        ARMAS_MATERIALES.MADERA: {
            Arma_nombre_key: "Espada de Madera",
            Arma_icono_key: "res://recursos/armas/Espadas/espadaDeMadera.png",
            Precio_key: 0
        },
        ARMAS_MATERIALES.OBSIDIANA: {
            Arma_nombre_key: "Espada de Obsidiana",
            Arma_icono_key: "res://recursos/armas/Espadas/espadaDeObsidiana.png",
            Precio_key: 1000
        },
        ARMAS_MATERIALES.DEMONIUM : {
            Arma_nombre_key: "Espada Demoniaca",
            Arma_icono_key: "res://recursos/armas/Espadas/espadaMaldita.png",
            Precio_key: 2000
        }
    },
    PERSONAJES_IDs.GUERRERO: {
        ARMAS_MATERIALES.MADERA:{
            Arma_nombre_key: "Mazo de trabajo",
            Arma_icono_key: "res://recursos/armas/Martillos/MazoDeMadera.png",
        },
        ARMAS_MATERIALES.HIERRO: {
            Arma_nombre_key: "Mazo dorado",
            Arma_icono_key: "res://recursos/armas/Martillos/MazoDorado.png",
        },
        ARMAS_MATERIALES.DIAMANTE: {
            Arma_nombre_key: "Mazo mata dragones",
            Arma_icono_key: "res://recursos/armas/Martillos/MazoMataDragones.png",
        },
        ARMAS_MATERIALES.OBSIDIANA: {
            Arma_nombre_key: "Mazo de Obsidiana",
            Arma_icono_key: "res://recursos/armas/Martillos/MazoDeObsidiana.png",
        },
        ARMAS_MATERIALES.DEMONIUM: {
            Arma_nombre_key: "Mazo Demoniaco",
            Arma_icono_key: "res://recursos/armas/Martillos/MazoMaldito.png",
        }
    },
    PERSONAJES_IDs.MAGA_OSCURA: {
        ARMAS_MATERIALES.MADERA: {
            Arma_nombre_key: "Vararita",
            Arma_icono_key: "res://recursos/armas/Varitas/VaritaDeMadera.png",
        },
        ARMAS_MATERIALES.HIERRO: {
            Arma_nombre_key: "Vara sandrienta dorada",
            Arma_icono_key: "res://recursos/armas/Varitas/VaritaDorada.png",
        },
        ARMAS_MATERIALES.DIAMANTE: {
            Arma_nombre_key: "Vara de los Dioses",
            Arma_icono_key: "res://recursos/armas/Varitas/VaritadeDiamante.png",
        },
        ARMAS_MATERIALES.OBSIDIANA: {
            Arma_nombre_key: "Varita Antigua",
            Arma_icono_key: "res://recursos/armas/Varitas/VartiaMataDemonios.png",
        },
        ARMAS_MATERIALES.DEMONIUM: {
            Arma_nombre_key: "Varita Demoniaca",
            Arma_icono_key: "res://recursos/armas/Varitas/VaritaDeLosMuertos.png",
        },
    },
    PERSONAJES_IDs.MAGA_BLANCA: {
        ARMAS_MATERIALES.MADERA:{
            Arma_nombre_key: "Baston Bendecido",
            Arma_icono_key: "res://recursos/armas/Bastones/bastónDeMadera.png",
        },
        ARMAS_MATERIALES.HIERRO: {
            Arma_nombre_key: "Baston De Sangre Divina",
            Arma_icono_key: "res://recursos/armas/Bastones/bastonDeSangreDivina.png",
        },
        ARMAS_MATERIALES.DIAMANTE: {
            Arma_nombre_key: "Baton Sagrado",
            Arma_icono_key: "res://recursos/armas/Bastones/BastónDeDiamante.png",
        },
        ARMAS_MATERIALES.OBSIDIANA: {
            Arma_nombre_key: "Baston de Obsidiana",
            Arma_icono_key: "res://recursos/armas/Bastones/BastónDeObsidiana.png",
        },
        ARMAS_MATERIALES.DEMONIUM: {
            Arma_nombre_key: "Baston Demoniaco",
            Arma_icono_key: "res://recursos/armas/Bastones/BastónMaldito.png",
            Precio_key : 900
        }
    },
    PERSONAJES_IDs.NINJA: {
        ARMAS_MATERIALES.MADERA: {
            Arma_nombre_key: "Daba De Madera",
            Arma_icono_key : "res://recursos/armas/Dagas/dagaMadera.png",
        },
        ARMAS_MATERIALES.HIERRO: {
            Arma_nombre_key: "Data De Hierro",
            Arma_icono_key: "res://recursos/armas/Dagas/dagaHierro.png",
        },
        ARMAS_MATERIALES.DIAMANTE: {
            Arma_nombre_key: "Data De Diamante",
            Arma_icono_key: "res://recursos/armas/Dagas/dagaDiamante.png",
        },
        ARMAS_MATERIALES.OBSIDIANA: {
            Arma_nombre_key: "Data De Obsidiana",
            Arma_icono_key: "res://recursos/armas/Dagas/dagaObsidiana.png",
        },
        ARMAS_MATERIALES.DEMONIUM: {
            Arma_nombre_key: "Data Maldita",
            Arma_icono_key: "res://recursos/armas/Dagas/dagaMaldita.png",
        }
    }
}