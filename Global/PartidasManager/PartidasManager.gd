extends Node

const partidas_key = "partidas"
const partidas_path = "user://Partidas.save"
const partida_vacio_key = "vacio"
const partida_tiempo_key = "tiempo"
const partida_sub_nivel_id_key = "sub_nivel_id"
const partida_grupo_key = "grupo_activo"

const partidaInfoRef = preload("res://Data/PartidaInfo.gd")

var partidas_cargadas = false

var data = {
	partidas_key : []
}

func _ready() -> void:
	_cargar_partidas()	

func _cargar_partidas() -> void:
	var file = File.new()
	if file.file_exists(partidas_path):
		var error = file.open(partidas_path,File.READ)
		if error == OK:
			var file_data = parse_json(file.get_as_text())
			if typeof(file_data) == TYPE_DICTIONARY:
				data = file_data
				partidas_cargadas = true
				Debugger.log("Partidas Cargadas", name)
			else:
				Debugger.log("Error al leer paridas", name)

func get_partidas_data() -> Dictionary:
	return data

func get_partidas_ids() ->Array:
	return data[partidas_key]

func guardar_partida_info(slot_id:int) -> void:
	if not slot_id in data[partidas_key]:
		data[partidas_key].append(slot_id)
	data[str(slot_id)] = _get_partida_info(slot_id)
	var file = File.new()
	var error = file.open(partidas_path,File.WRITE)
	if error == OK:
		file.store_line(to_json(data))
	else:
		Debugger.log("Error al intentar crear el archivo %s" % partidas_path, name)
	file.close()

func _get_partida_info(slot_id) -> Dictionary:
	var niveles_completados = GameManager.partida.get_niveles_completados()
	var sub_nivel_id = niveles_completados.back() if not niveles_completados.empty() else -1
	var grupo_activo = []
	for jugador in GameManager.partida.get_grupo_activo():
		grupo_activo.append(GameDefinitions.Personajes_nombres_by_escena[jugador])
	
	var mi_tiempo = TimeManager.get_tiempo_crudo()

	var partida_info = partidaInfoRef.new(
		false, #vacio
		sub_nivel_id,
		grupo_activo,
		mi_tiempo
	) 
	return partida_info.get_data()

func get_tiempo_partida(slot_id:int) -> float:
	if data.has(str(slot_id)):
		return data[str(slot_id)][partida_tiempo_key]
	else:
		return 0.0
