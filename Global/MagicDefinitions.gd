extends Node

const fulgur_key:String = "Fulgur"
const sana_key:String = "Sana" 
const ignis_key:String = "Ignis"
const aqua_key:String = "Aqua"

const fuerza = "fuerza"
const gasto = "gasto"

const ERROR_MSG_WRONG_VALUE = "Error: Una magia esta mal asignada" + \
					" ya que no esta definida en las magias disponibles en MagicDefinitions, magias: "

var magias_disponibles = [fulgur_key,sana_key,ignis_key,aqua_key]

export(PackedScene) var fulgur_anim_ref = null
export(PackedScene) var sana_anim_ref = null
export(PackedScene) var ignis_anim_ref = null
export(PackedScene) var aqua_anim_ref = null

class Magia_stats:
	var nombre_magia:String
	var fuerza:int
	var gasto:int

	func _init(miNombre:String, miFuerza: int,miGasto:int):
		self.nombre_magia = miNombre
		self.fuerza = miFuerza
		self.gasto = miGasto

var fulgur = Magia_stats.new(fulgur_key,40,5)
var sana = Magia_stats.new(sana_key,-50,10)
var aqua = Magia_stats.new(aqua_key,50,6)
var ignis = Magia_stats.new(ignis_key,80,10)

var Magias_dicc = {
	fulgur_key: fulgur,
	ignis_key: ignis,
	sana_key: sana,
	aqua_key: aqua
}

func get_gasto(magia_key:String) -> int:
	for magia in magias_disponibles:
		if magia == magia_key:
			return Magias_dicc[magia].gasto
	return -1

func assert_magias_validas(magias:Array) -> void:
	assert(todas_las_magias_validas(magias), ERROR_MSG_WRONG_VALUE + String(magias))

func assert_magia_valida(magia:String) -> void:
	assert(magia in magias_disponibles,ERROR_MSG_WRONG_VALUE + magia)

func todas_las_magias_validas(magias:Array) -> bool:
	for magia in magias:
		if not magia in magias_disponibles:
			return false
	return true
