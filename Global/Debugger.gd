extends Node


func log(message:String, path:String) -> void:
	print("[%s]::" % path  + message)

func log_error(message:String, path:String) -> void:
	assert(false, "[%s]::%s" % [path, message])
