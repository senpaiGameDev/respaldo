extends CanvasLayer

signal scene_changed
signal scene_added
signal fade_in_completed
signal fade_out_completed

onready var animation = get_node("Animation")

func cambiar_escena(scene,delay = 0.25) -> void:
	yield(get_tree().create_timer(delay),"timeout")
	animation.play("Fade")

	yield(animation,"animation_finished")
	emit_signal("fade_in_completed")

	if typeof(scene) == TYPE_STRING:
		assert(get_tree().change_scene(scene) == OK)
	else:
		assert(get_tree().change_scene_to(scene) == OK)
	animation.play_backwards("Fade")
	emit_signal("scene_changed")

	yield(animation,"animation_finished")
	emit_signal("fade_out_completed")


func agregar_escena(nodo:Node,delay = 0.25) -> void:
	yield(get_tree().create_timer(delay),"timeout")
	animation.play("Fade")

	yield(animation,"animation_finished")
	emit_signal("fade_in_completed")

	get_tree().get_root().call_deferred("add_child",nodo)
	animation.play_backwards("Fade")
	emit_signal("scene_added")

	yield(animation,"animation_finished")
	emit_signal("fade_out_completed")



func fade_in_out_animation(delay = 0.25) -> void:
	animation.play("Fade")

	yield(animation,"animation_finished")
	emit_signal("fade_in_completed")

	yield(get_tree().create_timer(delay),"timeout")
	animation.play_backwards("Fade")

	yield(animation,"animation_finished")
	emit_signal("fade_out_completed")
	
