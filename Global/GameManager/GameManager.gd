extends Node

signal partida_cargada
signal empezar_nueva_partida

const partida_ref = preload("res://Data/Partida.gd")
const inventario_ref = preload("res://Data/Inventario.gd")

const arena_de_batalla_scn = preload("res://sistema_de_combate/Arena_de_batalla/Arena_de_batalla/Arena_de_batalla.tscn")
const game_over_scn = preload("res://Interfaces/Game Over/GameOver.tscn")

var partida
var on_batalla
var partida_id = -1

func _ready() -> void:
	partida = partida_ref.new()
	ItemDefinitions.cargar_articulos_en_memoria()

func nueva_partida() -> void:
	partida = partida_ref.new()
	emit_signal("empezar_nueva_partida")

func cargar(slot_id:int) -> void:
	partida.cargar_partida(slot_id)
	partida_id = slot_id
	emit_signal("partida_cargada")

func guardar(slot_id:int) -> void:
	partida.guardar_partida(slot_id)
	PartidasManager.guardar_partida_info(slot_id)
	partida_id = slot_id

func get_grupo_activo() -> Array:
	return partida.get_grupo_activo()