extends Node

signal articulos_cargados

class Articulo_data:
	var id:int
	var nombre:String
	var cantidad:int
	var path:String
	var textura_path:String
	var articulo_obj: Articulo 
	var descripcion:String

	func _init(n_id,n_nombre, n_cantidad, n_path, n_textura_path:String,articulo_des) -> void:
		self.id = n_id
		self.nombre = n_nombre
		self.cantidad = n_cantidad
		self.path = n_path
		self.textura_path = n_textura_path
		self.descripcion = articulo_des

	func instanciar_articulo() -> void:
		assert(path != "","El articulo" + self.nombre + " no tiene una ruta de su objeto asignada")
		self.articulo_obj = load(self.path).instance()
		self.articulo_obj.set_atributos(self.id,self.nombre,self.cantidad)


var D_PosionCH_HP = Articulo_data.new(
	1,"Posion HP chica", 50, 
	"res://Articulos/Posiones/HP/Posion_HP.tscn",
	"res://recursos/Articulos/item6.png",
	"Este articulo cura el hp del usaurio 50 puntos de vida"
	)

var D_PosionM_HP = Articulo_data.new(
	2,"Posion HP mediana", 400,
	"res://Articulos/Posiones/HP/Posion_HP.tscn",
	"res://recursos/Articulos/item19.png",
	"Este articulo cura el hp del usuario 400 puntos de vida"
	)

var D_PosionG_HP = Articulo_data.new(
	3,"Posion HP grande", 1800,
	"res://Articulos/Posiones/HP/Posion_HP.tscn",
	"res://recursos/Articulos/item18.png",
	"Este articulo cura el hp del usuario 1800 puntos de vida"
	)

var D_PosionCH_MP = Articulo_data.new(
	4,"Posion MP chica",20,
	"res://Articulos/Posiones/MP/Posion_MP.tscn",
	"res://recursos/Articulos/item15.png",
	"Este articulo recupera 20 puntos del mp del usuario"
	)

var D_PosionM_MP = Articulo_data.new(
	5,"Posion MP mediana",80,
	"res://Articulos/Posiones/MP/Posion_MP.tscn",
	"res://recursos/Articulos/item17.png",
	"Este articulo recupera 80 puntos del mp del usuario"
	)

var D_PosionG_MP = Articulo_data.new(
	6,"Posion MP grande",150,
	"res://Articulos/Posiones/MP/Posion_MP.tscn",
	"res://recursos/Articulos/item16.png",
	"Este articulo recupera 150 puntos del mp del usuario"
	)

var D_Elixir = Articulo_data.new(
	7,"Elixir", 0,
	"res://Articulos/Especiales/Elixir.tscn",
	"res://recursos/Articulos/item13.png",
	"Este articulo recupera todo el hp y el mp del usuario"
	)

var D_Hoja_de_Fenix = Articulo_data.new(
	8,"Hoja de fenix", 0,
	"res://Articulos/Especiales/Hoja_de_fenix.tscn",
	"res://recursos/Articulos/item8.png",
	"Este articulo revivie al usuario"
	)

var articulos_disponibles = [
	D_PosionCH_HP,D_PosionM_HP,D_PosionG_HP,
	D_PosionCH_MP,D_PosionM_MP,D_PosionG_MP,
	D_Elixir, D_Hoja_de_Fenix
]

func cargar_articulos_en_memoria() -> void:
	for articulo in articulos_disponibles:
		articulo.instanciar_articulo()
	emit_signal("articulos_cargados")

func _comprobar_articulos_cargados() -> bool:
	for articulo in articulos_disponibles:
		if articulo.articulo_obj == null:
			return false
	return true

func existe_articulo(articulo_id:int) -> bool:
	for articulo in articulos_disponibles:
		if articulo.id == articulo_id:
			return true
	return false

func get_articulo_data_by_id(id:int) -> Articulo_data:
	for articulo in articulos_disponibles:
		if articulo.id == id:
			return articulo
	return null

func get_articulo_data_by_nombre(nombre:String) -> Articulo_data:
	for articulo in articulos_disponibles:
		if articulo.nombre == nombre:
			return articulo
	return null

func descargar_articulos() -> void:
	for articulo in articulos_disponibles:
		articulo.liberar_memoria()
