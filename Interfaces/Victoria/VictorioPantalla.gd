extends Panel

signal continuar

onready var exp_label = get_node("VBoxContainer/ExperienciaGanada")
onready var jugadores_contenedor = get_node("VBoxContainer/SubieronNivel")
onready var monedas_label = get_node("VBoxContainer/MonedasGanadas")
onready var bton_continuar = get_node("VBoxContainer/TextureButton")

var jugadores_labels

func _ready() -> void:
	bton_continuar.connect("pressed",self,"_on_continuar_pressed")
	jugadores_labels = jugadores_contenedor.get_children()

func _on_continuar_pressed() -> void:
	visible = false
	emit_signal("continuar")

func set_info(exp_ganada:int,jugadores:Array,monedas_ganadas:int) -> void:
	exp_label.text = "Exp ganada: %s" % str(exp_ganada)
	for i in range(jugadores.size()):
		jugadores_labels[i].text = "%s ha subido a nivel %d" % [jugadores[i]["nombre"], jugadores[i]["nivel"]]
	monedas_label.text = "Monedas ganadas: %s" % str(monedas_ganadas)

func show() -> void:
	visible = true
	bton_continuar.grab_focus()	



