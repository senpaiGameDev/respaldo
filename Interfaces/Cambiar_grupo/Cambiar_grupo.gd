extends Control

signal salir(this)

onready var grupo_activo_contenedor = get_node("HBoxContainer/Grupo_activo")
onready var banca_contenedor = get_node("HBoxContainer/Banca")

var banca_btns 
var activos_btns
var activo_seleccionado = ""
var activo_selec_index
var banca_seleccionado = ""
var banca_seleccion_index

func _ready() -> void:
	banca_btns = banca_contenedor.get_children()
	activos_btns = grupo_activo_contenedor.get_children()
	banca_btns[0].grab_focus()
	asignar_personajes()
	for i in range(banca_btns.size()):
		banca_btns[i].connect("pressed",self, "_on_banca_pressed", [i])
	for i in range(activos_btns.size()):
		activos_btns[i].connect("pressed",self,"_on_activos_pressed",[i])

func _process(delta: float) -> void:
	if Input.is_action_just_pressed("ui_cancel"):
		_salir()

func asignar_personajes() -> void:
	var grupo_activo  = GameManager.get_grupo_activo()
	for i in range(grupo_activo.size()):
		activos_btns[i].set_personaje(grupo_activo[i])
	var indice = 0
	for personaje in GameDefinitions.lista_jugadores:
		if not personaje in grupo_activo:
			banca_btns[indice].set_personaje(personaje)
			indice += 1
			indice = clamp(indice,0,banca_btns.size() - 1)


func _on_activos_pressed(indice:int) -> void:
	if activo_seleccionado != "":
		for btn in activos_btns:
			btn.resetear_color()
		_resetar_activos()
		return

	activo_seleccionado = activos_btns[indice].personaje_escena
	activo_selec_index = indice
	if banca_seleccionado != "" and banca_seleccion_index != -1:
		activos_btns[indice].actualizar(banca_seleccionado)
		banca_btns[banca_seleccion_index].actualizar(activo_seleccionado)
		swap()

func _on_banca_pressed(indice:int) -> void:
	if banca_seleccionado != "":
		for btn in banca_btns:
			btn.resetear_color()
		_resetar_banca()
		return

	banca_seleccionado = banca_btns[indice].personaje_escena
	banca_seleccion_index = indice
	if activo_seleccionado != "" and activo_selec_index != -1:
		banca_btns[indice].actualizar(activo_seleccionado)
		activos_btns[activo_selec_index].actualizar(banca_seleccionado)
		swap()

func _resetar_activos() -> void:
	activo_seleccionado = ""
	activo_selec_index = -1

func _resetar_banca() -> void:
	banca_seleccionado = ""
	banca_seleccion_index = -1

func swap() -> void:
	GameManager.partida.cambiar_grupo(banca_seleccionado,activo_seleccionado)
	_resetar_activos()
	_resetar_banca()
	Debugger.log("nueva partida: %s" %str(GameManager.get_grupo_activo()), name)

func _salir() -> void:
	emit_signal("salir",self)