extends TextureButton

export(Color) var color_seleccion 

onready var personaje_nombre = get_node("HBoxContainer/Nombre")
onready var icono = get_node("HBoxContainer/Icono")

var personaje_escena

var color_original

func _ready() -> void:
	color_original = modulate
	connect("button_down",self, "_on_pressed")

func _on_pressed() -> void:
	modulate = color_seleccion

func set_personaje(personaje_escena_n:String) -> void:
	personaje_escena = personaje_escena_n
	var nombre = GameDefinitions.Personajes_nombres_by_escena[personaje_escena]
	var icono_path = GameDefinitions.Personajes_iconos_by_escena[personaje_escena]
	icono.texture = load(icono_path)
	personaje_nombre.text = nombre

func resetear_color() -> void:
	modulate = color_original

func actualizar(personaje_escena_n:String) -> void:
	modulate = color_original
	set_personaje(personaje_escena_n)

