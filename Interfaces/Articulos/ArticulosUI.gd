extends Control

signal salir(this)

const articuloUI_ref = preload("res://Interfaces/Articulos/ArticuloUI/ArticuloUI.tscn")

onready var articulos_contenedor = get_node("ScrollContainer/Articulos")

func _ready() -> void:
	for articulo_id in GameManager.partida.inventario.get_articulos().keys():
		crear_articulo(articulo_id)
	articulos_contenedor.get_child(0).grab_focus()

func _process(delta: float) -> void:
	if Input.is_action_just_pressed("ui_cancel"):
		_salir()

func crear_articulo(articulo_id) -> void:
	var articulo = articuloUI_ref.instance()
	articulo.set_articulo(articulo_id)
	articulos_contenedor.add_child(articulo)

func _salir() -> void:
	emit_signal("salir",self)
