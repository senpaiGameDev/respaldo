extends TextureButton

onready var nombre_label = get_node("HBoxContainer/HBoxContainer/Nombre")
onready var cantidad_label = get_node("HBoxContainer/Cantidad")
onready var icono = get_node("HBoxContainer/Icono")
onready var descripcion_label = get_node("HBoxContainer/HBoxContainer/Descripcion")

var articulo_id
var articulo_data
var inicializado = false

func set_articulo(articulo_id_n:int) -> void:
	articulo_id = articulo_id_n
	articulo_data = ItemDefinitions.get_articulo_data_by_id(articulo_id)
	inicializado = true

func _ready() -> void:
	if inicializado:
		nombre_label.text = articulo_data.nombre
		cantidad_label.text = "X " + str(GameManager.partida.inventario.obtener_cantidad_articulo(articulo_id))
		icono.texture = load(articulo_data.textura_path)	
		descripcion_label.text = articulo_data.descripcion

