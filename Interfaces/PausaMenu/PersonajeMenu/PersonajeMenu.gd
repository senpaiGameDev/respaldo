extends Panel

const atributos_ref = preload("res://Interfaces/Atributos/Atributos.tscn")
const equipamiento_ref = preload("res://Interfaces/Equipamiento/Equipamiento.tscn")

onready var btn_atributos = get_node("HBoxContainer/Opciones/Atributos")
onready var btn_equipo = get_node("HBoxContainer/Opciones/Equipamiento")
onready var icono = get_node("HBoxContainer/Icono")
onready var nombre = get_node("HBoxContainer/Nombre")

var jugador_id
var icono_path
var jugador_nombre

func set_jugador(jugador_escena:String) -> void:
	assert(GameDefinitions.Personajes_nombres_by_escena.has(jugador_escena),
		"[%s]::El jugador: %s no es valido" % [name, jugador_escena])
	assert(GameDefinitions.Personajes_iconos_by_escena.has(jugador_escena),
		"[%s]:: El jugador: %s no es valido" % [name,jugador_escena])
	assert(GameDefinitions.Personajes_ID.has(jugador_escena),
		"[%s]:: El jugador: %s no es valido" % [name,jugador_escena])
	icono_path = GameDefinitions.Personajes_iconos_by_escena[jugador_escena]
	jugador_nombre = GameDefinitions.Personajes_nombres_by_escena[jugador_escena] 
	icono.texture = load(icono_path)
	nombre.text = jugador_nombre
	jugador_id = GameDefinitions.Personajes_ID[jugador_escena]


func _ready() -> void:
	btn_atributos.connect("pressed",self, "_on_atributos_pressed")
	btn_equipo.connect("pressed",self,"_on_equipo_pressed")


func _on_atributos_pressed() -> void:
	var atributos = atributos_ref.instance()
	atributos.set_data(jugador_id, icono_path, jugador_nombre)
	atributos.connect("salir",self, "on_atributos_salir")
	owner.add_child(atributos)

func on_atributos_salir(atributos) -> void:
	atributos.queue_free()
	btn_atributos.grab_focus()

func _on_equipo_pressed() -> void:
	var equipamiento = equipamiento_ref.instance()
	equipamiento.set_data(jugador_id,icono_path,jugador_nombre)
	equipamiento.connect("salir",self, "_on_equipamiento_salir")
	owner.add_child(equipamiento)

func _on_equipamiento_salir(equipamientoUI) -> void:
	equipamientoUI.queue_free()
	btn_equipo.grab_focus()

func enfocar() -> void:
	btn_atributos.grab_focus()
