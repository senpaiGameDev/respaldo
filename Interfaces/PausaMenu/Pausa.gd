extends Control

const guardar_menu_ref = preload("res://Interfaces/Guardar/Guardar.tscn")
const articulos_menu_ref = preload("res://Interfaces/Articulos/ArticulosUI.tscn")
const cambiar_jugadores_ref = preload("res://Interfaces/Cambiar_grupo/Cambiar_grupo.tscn")
const menu_princial_path = "res://Interfaces/MenuPrincipal/MenuPrincipal.tscn"

onready var personajes_contenedor = get_node("Contenedor/Personajes")
onready var btn_guardar = get_node("Contenedor/Opciones/GuardarPartida")
onready var btn_articulos = get_node("Contenedor/Opciones/Articulos")
onready var btn_cambiar_grupo = get_node("Contenedor/Opciones/CambiarGrupo")
onready var btn_salir = get_node("Contenedor/Opciones/Salir")
onready var tiempo_label = get_node("Partida info/Tiempo")
onready var monedas_label = get_node("Partida info/Monedas/Label")

var personajes_ui
var guardar_menu
var hijos_originales_cuenta

func _ready() -> void:
	hijos_originales_cuenta = get_child_count()
	GameDefinitions.on_gui = true
	personajes_ui = personajes_contenedor.get_children()
	_config_personajes()
	monedas_label.text = str(GameManager.partida.inventario.get_monedas())
	btn_cambiar_grupo.grab_focus()
	conectar_botones()

func _config_personajes() -> void:
	var personajes = GameManager.get_grupo_activo()
	assert(personajes.size() == personajes_ui.size(),
		"[%s]:: Error, existe una desigualdad entre el grupo y la interfaz" % name)
	for index in range(personajes.size()):
		personajes_ui[index].set_jugador(personajes[index])

func conectar_botones() -> void:
	btn_guardar.connect("pressed", self, "_on_btn_guardar_pressed")
	btn_cambiar_grupo.connect("pressed",self,"_on_cambiar_grupo_pressed")
	btn_articulos.connect("pressed",self,"_on_articulos_pressed")
	btn_salir.connect("pressed",self,"_on_btn_salir_pressed")

func _on_btn_guardar_pressed() -> void:
	guardar_menu = guardar_menu_ref.instance()
	guardar_menu.connect("salir",self,"_on_menu_guardar_salir")
	add_child(guardar_menu)

func _on_cambiar_grupo_pressed() -> void:
	var cambiar_menu = cambiar_jugadores_ref.instance()
	cambiar_menu.connect("salir",self,"_on_cambiar_grupo_salir")
	add_child(cambiar_menu)

func _on_articulos_pressed() -> void:
	var articulos_menu = articulos_menu_ref.instance()
	articulos_menu.connect("salir",self,"_on_articulos_menu_salir")
	add_child(articulos_menu)

func _on_btn_salir_pressed() -> void:
	_salir()
	SceneManager.cambiar_escena(menu_princial_path)

func _on_menu_guardar_salir(menu_guardar) -> void:
	remove_child(menu_guardar)
	menu_guardar.queue_free()
	btn_guardar.grab_focus()

func _on_cambiar_grupo_salir(cambiar_menu) -> void:
	actualizar_jugadores()
	remove_child(cambiar_menu)
	cambiar_menu.queue_free()
	btn_cambiar_grupo.grab_focus()

func _on_articulos_menu_salir(articulos_menu) -> void:
	remove_child(articulos_menu)
	articulos_menu.queue_free()
	btn_articulos.grab_focus()

func _process(_delta: float) -> void:
	mover_en_interfaz()
	_set_tiempo_partida()

func mover_en_interfaz() -> void:
	if Input.is_action_just_pressed("ui_cancel"):
		if get_child_count() == hijos_originales_cuenta:
			_salir()

func _salir() -> void:
	GameDefinitions.on_gui = false
	queue_free()
	get_tree().paused = false

func _set_tiempo_partida() -> void:
	tiempo_label.text = "Tiempo: " + TimeManager.get_tiempo()

func actualizar_jugadores() -> void:
	_config_personajes()
