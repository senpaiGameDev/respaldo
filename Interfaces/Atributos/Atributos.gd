extends Control

signal salir(this)

const EstadisticasRaw = preload("res://Data/EstadisticasRaw.gd")

onready var icono = get_node("Encabezado/PanelContainer2/Icono")
onready var nombre_label = get_node("Encabezado/PanelContainer/Nombre")

onready var siguiente_nivel_bar = get_node("VBoxContainer/SiguienteNivel/ProgressBar")

onready var nivel_label = get_node("VBoxContainer/Nivel")
onready var hp_label = get_node("VBoxContainer/Hp")
onready var mp_label = get_node("VBoxContainer/MP")
onready var fuerza_label = get_node("VBoxContainer/Fuerza")
onready var poder_magico_label = get_node("VBoxContainer/PoderMagico")
onready var resistencia_label = get_node("VBoxContainer/Resistencia")
onready var resistencia_magica_label = get_node("VBoxContainer/ResistenciaMagica")
onready var experiencia_label = get_node("VBoxContainer/Experiencia")

var jugador_id
var data
var jugador_nombre
var icono_path

func _ready() -> void:
	if jugador_id != null:
		_inicializar()

func _process(delta: float) -> void:
	if Input.is_action_just_pressed("ui_cancel"):
		emit_signal("salir",self)

func _inicializar() -> void:
	icono.texture = load(icono_path)
	nombre_label.text = jugador_nombre
	nivel_label.text = "Nivel : %s" % str(data['nivel'])
	hp_label.text = "HP : %s" % str(data["hp_max"])
	mp_label.text = "MP : %s" % str(data["mp_max"])
	fuerza_label.text = "Fuerza : %s" % str(data["fuerza"])
	poder_magico_label.text = "Poder Magico : %s" % str(data['poder_magico'])
	resistencia_label.text = "Resistencia : %s" % str(data["resistencia"])
	resistencia_magica_label.text = "Resistencia Magica : %s" % str(data["resistencia_magica"])
	experiencia_label.text = "Experiencia : %s" % str(data['experiencia'])
	siguiente_nivel_bar.max_value = siguiente_nivel(data['nivel'])
	siguiente_nivel_bar.value = data['experiencia']

func siguiente_nivel(nivel_n:int) -> int:
	var siguiente_nivel = nivel_n + 1
	return int(round(( 4 * pow((siguiente_nivel + 1), 3)) / 5)) 

func set_data(jugador_id_n:int,icono_n:String,nombre:String) -> void:
	jugador_id = jugador_id_n
	data = GameManager.partida.configuracion_jugadores.get_stats_jugador(jugador_id)
	icono_path = icono_n
	jugador_nombre = nombre
