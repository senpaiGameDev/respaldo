extends Button

export(int) var slot_id

const DateTime = preload("res://Utilidades/DateTime.gd")

onready var partida_info_container = get_node("partidaInfo")
onready var partida_info_tiempo = get_node("partidaInfo/Tiempo")
onready var partida_info_titulo = get_node("partidaInfo/Titulo")
onready var partida_info_grupo = get_node("partidaInfo/Grupo")

#partida info
var vacio:bool = true 
var sub_nivel_id:int
var grupo = []
var tiempo
var iniciado = false

func set_partida_info(vacio_n:bool,subnivel_id_n:int,grupo_n:Array,tiempo_n:float) -> void:
	vacio = vacio_n
	sub_nivel_id = subnivel_id_n
	grupo = grupo_n.duplicate()
	tiempo = tiempo_n
	if iniciado:
		sincronizar_con_partida()

func _ready():
	iniciado = true
	if vacio:
		text = "Vacio"
		partida_info_container.visible = false
	else:
		sincronizar_con_partida()

func sincronizar_con_partida() -> void:
		text = ""
		partida_info_container.visible = true
		set_titulo()
		set_grupo()
		set_tiempo()


func set_titulo() -> void:
	if GameDefinitions.sub_niveles_info_by_id.has(sub_nivel_id):
		var sub_nivel_nombre = GameDefinitions.sub_niveles_info_by_id[sub_nivel_id]["nombre"]
		var nivel_nombre = GameDefinitions.sub_niveles_info_by_id[sub_nivel_id]["nivel"]
		var titulo = "%s : %s" % [nivel_nombre,sub_nivel_nombre]
		partida_info_titulo.text = titulo
	else:
		partida_info_grupo.text = "----"

func set_grupo() -> void:
	for index in grupo.size():
		partida_info_grupo.get_child(index).text = grupo[index]

func set_tiempo() -> void:
	var datetime = DateTime.new(tiempo)
	partida_info_tiempo.text = datetime.get_text()
