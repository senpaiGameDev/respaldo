extends Control

signal salir(menuGuardar)
const scroll_increment = 15

onready var btns_guardas_contenedor = get_node("ScrollContenedor/partidasContenedor")
onready var guardar_panel = get_node("Guardando")
onready var dialog_popup = get_node("ConfirmationDialog")
onready var scrool_container = get_node("ScrollContenedor")
onready var guardado_exitoso_popup = get_node("GuardadoExitoso")

var partidas_data
var partidas_keys
var botones_guardar
var valor_scroll

func _ready() -> void:
	partidas_data = PartidasManager.get_partidas_data()
	partidas_keys = PartidasManager.get_partidas_ids()
	botones_guardar = btns_guardas_contenedor.get_children()
	botones_guardar[0].grab_focus()
	asignar_partidas()
	for btn_guardar in botones_guardar:
		btn_guardar.connect("pressed",self,"on_btn_guardar_pressed",[btn_guardar])

func _process(delta: float) -> void:
	if Input.is_action_just_pressed("ui_cancel"):
		_salir()

func _salir() -> void:
	emit_signal("salir",self)

func asignar_partidas() -> void:
	for partida_id in partidas_keys:
		for btn_guardar in botones_guardar:
			if btn_guardar.slot_id == partida_id:
				var partida = partidas_data[str(partida_id)]
				var vacio = partida[PartidasManager.partida_vacio_key]
				var sub_nivel_id = partida[PartidasManager.partida_sub_nivel_id_key]
				var grupo = partida[PartidasManager.partida_grupo_key]
				var tiempo = partida[PartidasManager.partida_tiempo_key]

				btn_guardar.set_partida_info(vacio,sub_nivel_id,grupo,tiempo)

func on_btn_guardar_pressed(btn_guardar) -> void:
	if btn_guardar.vacio:
		_guardar(btn_guardar.slot_id)
	else:
		_lanzar_popup(btn_guardar.slot_id)
		
func _lanzar_popup(slot_id) -> void:
	dialog_popup.connect("confirmed",self, "_on_popup_confirmed",[slot_id])
	dialog_popup.popup_centered_minsize()

func _on_popup_confirmed(slot_id:int) -> void:
	_guardar(slot_id)

func _guardar(slot_id) -> void:
	guardar_panel.visible = true
	GameManager.guardar(slot_id)
	asignar_partidas()
	yield(get_tree().create_timer(1),"timeout")
	guardar_panel.visible = false
	guardado_exitoso_popup.popup_centered()
