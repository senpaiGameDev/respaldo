extends TextureButton

signal seleccionar(this)

export(Color) var color_equipada
const Armas_stats = preload("res://sistema_de_combate/Armas/Data.gd")
const Fuerza_key = "fuerza"
const Magia_key = "magia"

onready var icono = get_node("HBoxContainer/Icono")
onready var nombre = get_node("HBoxContainer/HBoxContainer/Nombre")
onready var atributos = get_node("HBoxContainer/HBoxContainer/Atributos")

var inicializado = false
var personaje_id
var arma_material

var arma_ui_data

var arma_fuerza
var arma_magia

var color_original
var equipada = false

func set_primitivas(personaje:int, arma_m:int) -> void:
	#assert(GameDefinitions.PERSONAJES_IDs.has(personaje),"[%s]::El Personaje %s no existe en GameDefinitions" % [name,str(personaje)])
	#assert(GameDefinitions.ARMAS_MATERIALES.has(arma_m),"[%s]:: El material %s no existe" % [name, str(arma_m)])
	#assert(GameDefinitions.Armas[personaje].has(arma_m),
	#	"[%s]:: Los datos del personaje %s con arma %s no existe" %[name,str(personaje_id), str(arma_m)])
	personaje_id  = personaje
	arma_material = arma_m
	_get_datos()

func _get_datos() -> void:
	arma_ui_data = GameDefinitions.Armas[personaje_id][arma_material]
	var arma_tipo = GameDefinitions.arma_por_jugador[personaje_id]
	var arma_data:Dictionary = Armas_stats.get_arma_stats(arma_tipo,arma_material)

	if arma_ui_data == null:
		Debugger.log( "Error al tratar de obtener datos de ui del personaje: " + \
		"%s arma: %s con material: %s" % [str(personaje_id),arma_tipo,str(arma_material)],name)

	if arma_data.empty():
		Debugger.log("Error al tratar de obtener los stats del arma: %s con material: %s" % [arma_tipo,arma_material],name)
	else:
		arma_fuerza = arma_data[Fuerza_key]
		arma_magia  = arma_data[Magia_key]
		inicializado = true

func _ready() -> void:
	color_original = modulate
	connect("button_down",self,"_on_button_down")
	if inicializado:
		icono.texture = load(arma_ui_data[GameDefinitions.Arma_icono_key])
		nombre.text = arma_ui_data[GameDefinitions.Arma_nombre_key]
		atributos.text = "Ataque: %s, Magia: %s" % [arma_fuerza, arma_magia]

func _on_button_down() -> void:
	emit_signal("seleccionar",self)

func equipar() -> void:
	equipada = true
	modulate = color_equipada

func desequipar() -> void:
	equipada = false
	modulate = color_original
