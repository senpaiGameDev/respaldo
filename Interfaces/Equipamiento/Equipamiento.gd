extends Control

signal salir(this)

const btn_arma_ref = preload("res://Interfaces/Equipamiento/ArmaUI/ArmaUI.tscn")
onready var armas_contenedor = get_node("ScrollContainer2/Armas")
onready var personaje_nombre_label = get_node("Encabezado/PanelContainer/Nombre")
onready var personaje_icono = get_node("Encabezado/PanelContainer2/Icono")
onready var scrollbar = get_node("ScrollContainer2")

var incremento = 50

var personaje_id
var armas 
var inicializado = false
var arma_seleccionada
var personaje_icono_path
var personaje_nombre

func set_data(personaje_id_n:int, icono_path_n:String, personaje_nombre_n:String) -> void:
	personaje_nombre = personaje_nombre_n
	personaje_icono_path = icono_path_n
	personaje_id = personaje_id_n
	armas = GameManager.partida.inventario.get_armas_de_jugador(personaje_id)
	inicializado = true

func _ready() -> void:
	if inicializado:
		_set_encabezado_info()
		_crear_armas_btns()
	armas_contenedor.get_child(0).grab_focus()

func _set_encabezado_info() -> void:
	personaje_icono.texture = load(personaje_icono_path)
	personaje_nombre_label.text = personaje_nombre

func _crear_armas_btns() -> void:
	for arma_material in armas:
		var btn_arma = btn_arma_ref.instance() 
		btn_arma.set_primitivas(personaje_id,arma_material)
		armas_contenedor.add_child(btn_arma)
		btn_arma.connect("seleccionar",self, "_on_seleccionar_arma")
		if GameManager.partida.configuracion_jugadores.get_arma_jugador(personaje_id) == arma_material:
			arma_seleccionada = btn_arma
			btn_arma.equipar()

func _on_seleccionar_arma(arma_ui) -> void:
	#equipar_esta_arma
	GameManager.partida.configuracion_jugadores.actualizar_arma_jugador(personaje_id,arma_ui.arma_material)
	arma_seleccionada.desequipar()
	arma_seleccionada = arma_ui
	arma_seleccionada.equipar()

func _process(delta: float) -> void:
	if Input.is_action_just_pressed("ui_cancel"):
		_salir()

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_down"):
		scrollbar.scroll_vertical += incremento
	elif event.is_action_pressed("ui_up"):
		scrollbar.scroll_vertical -= incremento

func _salir() -> void:
	emit_signal("salir",self)
