extends Control

signal salir(this)

onready var scroll = get_node("ScrollContainer")

var incremento = 30

func _ready() -> void:
	scroll.grab_focus()

func _process(delta: float) -> void:
	if Input.is_action_just_pressed("ui_cancel"):
		_salir()
	if Input.is_action_pressed("ui_down"):
		scroll.scroll_vertical += incremento
	elif Input.is_action_pressed("ui_up"):
		scroll.scroll_vertical -= incremento

func _salir() -> void:
	emit_signal("salir",self)
