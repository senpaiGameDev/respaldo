extends TextureButton

signal articulo_comprado
signal on_popup(valor)

enum ItemsIDs {
	POSION_CH_HP = 1,
	POSION_M_HP = 2,
	POSION_G_HP = 3,
	POSION_CH_MP = 4,
	POSION_M_MP = 5,
	POSION_G_MP = 6,
	ELIXIR = 7,
	HOJA_DE_FENIX = 8
 }

export(ItemsIDs) var item_id = 1
export(int) var precio:int

onready var articulo_icono = get_node("HBoxContainer/Icono")
onready var articulo_nombre = get_node("HBoxContainer/HBoxContainer/Nombre")
onready var articulo_descripcion = get_node("HBoxContainer/HBoxContainer/Descripcion")
onready var precio_label = get_node("HBoxContainer/HBoxContainer/Precio/Label")
onready var confirmar_compra_popup = get_node("ConfirmarCompraPopup")
onready var monedas_insuficientes_popup = get_node("MonedasInsuficientes")


func _ready() -> void:
	var articulo_data = ItemDefinitions.get_articulo_data_by_id(item_id)
	articulo_icono.texture = load(articulo_data.textura_path)
	articulo_nombre.text = articulo_data.nombre
	articulo_descripcion.text = articulo_data.descripcion
	precio_label.text = str(precio)
	confirmar_compra_popup.connect("confirmed",self, "_on_compra_confirmed")
	confirmar_compra_popup.connect("popup_hide",self,"_on_popup_hide")
	monedas_insuficientes_popup.connect("popup_hide",self,"_on_popup_hide")
	connect("button_down",self,"_on_button_down")

func _on_button_down() -> void:
	if GameManager.partida.inventario.get_monedas() >= precio:
		emit_signal("on_popup",true)
		confirmar_compra_popup.popup_centered()
	else:
		emit_signal("on_popup",true)
		monedas_insuficientes_popup.popup_centered()

func _on_popup_hide() -> void:
	emit_signal("on_popup",false)

func _on_compra_confirmed() -> void:
	GameManager.partida.inventario.quitar_monedas(precio)
	GameManager.partida.inventario.agregar_articulo(item_id,1)
	emit_signal("articulo_comprado")
