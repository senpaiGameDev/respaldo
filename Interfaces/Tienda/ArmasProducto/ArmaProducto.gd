extends TextureButton

signal arma_comprada
signal on_popup(valor)

const Armas_data = preload("res://sistema_de_combate/Armas/Data.gd")
enum PERSONAJES_IDs {CABALLERO = 0, GUERRERO = 1, MAGA_BLANCA = 2, MAGA_OSCURA = 3, NINJA = 4}
enum ARMAS_MATERIALES { HIERRO,DIAMANTE,MADERA,OBSIDIANA,DEMONIUM }

export(PERSONAJES_IDs) var personaje_id
export(ARMAS_MATERIALES) var arma_material
export(int) var precio:int = 10

onready var personaje_icono = get_node("HBoxContainer/PersonajeIcono")
onready var arma_icono = get_node("HBoxContainer/AramaIcono")
onready var arma_nombre_label = get_node("HBoxContainer/especificaciones/Nombre")
onready var arma_atributos_label = get_node("HBoxContainer/especificaciones/Stats")
onready var precio_label = get_node("HBoxContainer/especificaciones/Precio/Label")
onready var obtenido_panel = get_node("Comprado")
onready var confirmar_compra_popup = get_node("ConfirmarCompraPopup")
onready var monedas_insuficientes_popup = get_node("MonedasInsuficientes")

var arma_ui_data

func _ready() -> void:
	set_ui()
	obtenido_panel.visible = GameManager.partida.inventario.set_tiene_arma(personaje_id,arma_material)
	connect("button_down",self, "_on_button_down")
	confirmar_compra_popup.connect("confirmed",self, "_on_compra_confirm")
	confirmar_compra_popup.connect("popup_hide",self,"_on_popup_hide")
	monedas_insuficientes_popup.connect("popup_hide",self,"_on_popup_hide")

func set_ui() -> void:
	arma_ui_data = GameDefinitions.Armas[personaje_id][arma_material]
	personaje_icono.texture = load(GameDefinitions.Personajes_iconos_by_id[personaje_id])
	arma_icono.texture = load(arma_ui_data[GameDefinitions.Arma_icono_key])
	arma_nombre_label.text = arma_ui_data[GameDefinitions.Arma_nombre_key]
	var arma_atributos = Armas_data.get_arma_stats(GameDefinitions.arma_por_jugador[personaje_id], arma_material)
	arma_atributos_label.text = "Ataque: %s, Magia: %s" % [str(arma_atributos["fuerza"]), str(arma_atributos["magia"])]
	precio_label.text = str(precio)

func _on_button_down() -> void:
	if obtenido_panel.visible: return
	if GameManager.partida.inventario.get_monedas() >= precio:
		emit_signal("on_popup",true)
		confirmar_compra_popup.popup_centered()
	else:
		emit_signal("on_popup",true)
		monedas_insuficientes_popup.popup_centered()

func _on_compra_confirm() -> void:
	GameManager.partida.inventario.quitar_monedas(precio)
	GameManager.partida.inventario.agregar_arma(personaje_id,arma_material)
	obtenido_panel.visible = true
	emit_signal("arma_comprada")

func _on_popup_hide() -> void:
	emit_signal("on_popup",false)
