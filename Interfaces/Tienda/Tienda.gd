extends Control

onready var armas_contenedor = get_node("HBoxContainer/Armas_scroll/Armas")
onready var articulos_contenedor = get_node("HBoxContainer/Articulos_scroll/Articulos")
onready var monedas_label = get_node("PanelContainer/HBoxContainer/Monedas/Cantidad")

var on_popup = false

func _ready() -> void:
	GameDefinitions.on_gui = true
	for arma in armas_contenedor.get_children():
		arma.connect("arma_comprada",self, "_on_arma_comprada")
		arma.connect("on_popup",self, "_on_producto_popup")
	for articulo in articulos_contenedor.get_children():
		articulo.connect("articulo_comprado",self, "_on_articulo_comprado")
		articulo.connect("on_popup",self, "_on_producto_popup")
	articulos_contenedor.get_child(0).grab_focus()
	actualizar_monedas_label()

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_cancel") and not on_popup:
		_salir()
	elif event.is_action_pressed("ui_left") and not on_popup:
		articulos_contenedor.get_child(0).grab_focus()
	elif event.is_action_pressed("ui_right") and not on_popup:
		armas_contenedor.get_child(0).grab_focus()


func _on_arma_comprada() -> void:
	actualizar_monedas_label()

func _on_articulo_comprado() -> void:
	actualizar_monedas_label()

func _on_producto_popup(valor) -> void:
	on_popup = valor

func actualizar_monedas_label() -> void:
	monedas_label.text = "%05d" % GameManager.partida.inventario.get_monedas()

func _salir() -> void:
	GameDefinitions.on_gui = false
	queue_free()
	get_tree().paused = false
	
