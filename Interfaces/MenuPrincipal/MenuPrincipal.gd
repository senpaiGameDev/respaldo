extends Control

export(PackedScene) var nueva_partida_scn
export(PackedScene) var cargar_partida_scn
export(PackedScene) var controles_scn

onready var botones = get_node("Buttons")
onready var nueva_partida_btn = get_node("Buttons/nueva_partida_btn")
onready var cargar_partida_btn = get_node("Buttons/cargar_btn")
onready var controles_btn = get_node("Buttons/Controles")
onready var salir_btn = get_node("Buttons/salir")


func _ready() -> void:
	GameDefinitions.on_gui = true
	nueva_partida_btn.connect("button_up",self, "_on_nueva_partida_btn_click")
	salir_btn.connect("button_up",self,"_on_salir_btn_click")
	cargar_partida_btn.connect("button_down",self,"_on_cargar_partida_btn_click")
	controles_btn.connect("button_down",self,"_on_controles_button_down")
	nueva_partida_btn.grab_focus()

func _on_salir_btn_click() -> void:
	get_tree().quit()

func _on_nueva_partida_btn_click() -> void:
	if nueva_partida_scn != null: 
		GameDefinitions.on_gui = false
		SceneManager.cambiar_escena(nueva_partida_scn)
		GameManager.nueva_partida()

func _on_cargar_partida_btn_click() -> void:
	var cargar_partida = cargar_partida_scn.instance()
	cargar_partida.connect("salir", self, "_on_cargar_salir")
	add_child(cargar_partida)

func _on_cargar_salir(menuCargar) -> void:
	remove_child(menuCargar)
	menuCargar.queue_free()
	nueva_partida_btn.grab_focus()

func _on_controles_button_down() -> void:
	var controles_menu = controles_scn.instance()
	controles_menu.connect("salir",self, "_on_controles_salir")
	add_child(controles_menu)

func _on_controles_salir(controles) -> void:
	remove_child(controles)
	controles.queue_free()
	nueva_partida_btn.grab_focus()





