extends Control

const menu_princial_path = "res://Interfaces/MenuPrincipal/MenuPrincipal.tscn"

onready var menu_principal = get_node("Buttons/menu_principal")
onready var salir = get_node("Buttons/salir")

func _ready() -> void:
    GameManager.on_batalla = false
    GameDefinitions.on_gui = true
    menu_principal.connect("button_up",self,"_on_menu_principal_click")
    salir.connect("button_up", self, "_on_salir_click")
    menu_principal.grab_focus()

func _on_salir_click() -> void:
    get_tree().quit()

func _on_menu_principal_click() -> void:
    SceneManager.cambiar_escena(menu_princial_path)

