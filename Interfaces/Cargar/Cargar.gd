extends Control

signal salir(cargarMenu)

export(PackedScene) var partida_btn
export(PackedScene) var mundo_scena

onready var partidas_contenedor = get_node("VBoxContainer/Partidas")
onready var popup = get_node("ConfirmationDialog")
onready var cargando_panel = get_node("Cargandopanel")
onready var sin_partidas = get_node("SinPartidas")

var partidas_data
var partidas_ids

func _ready() -> void:
	partidas_data = PartidasManager.get_partidas_data()
	partidas_ids = PartidasManager.get_partidas_ids()
	for partida in partidas_ids:
		_crear_partida_btn(partida)
	if partidas_contenedor.get_child_count() > 0:
		partidas_contenedor.get_child(0).grab_focus()
	else:
		sin_partidas.visible = true

func _process(delta: float) -> void:
	if Input.is_action_just_pressed("ui_cancel"):
		_salir()

func _salir() -> void:
	emit_signal("salir",self)
		
func _crear_partida_btn(partida_id) -> void:
	var btn_guardar = partida_btn.instance()

	Debugger.log("Partida: %s " % partidas_data, name)
	var partida = partidas_data[str(partida_id)]
	var vacio = partida[PartidasManager.partida_vacio_key]
	var sub_nivel_id = partida[PartidasManager.partida_sub_nivel_id_key]
	var grupo = partida[PartidasManager.partida_grupo_key]
	var tiempo = partida[PartidasManager.partida_tiempo_key]

	btn_guardar.set_partida_info(vacio,sub_nivel_id,grupo,tiempo)
	btn_guardar.connect("pressed",self, "_on_cargar_partida", [partida_id])
	partidas_contenedor.add_child(btn_guardar)


func _on_cargar_partida(partida_id:int) -> void:
	popup.connect("confirmed",self, "_on_cargar_partida_confirmed", [partida_id])
	popup.popup()

func _on_cargar_partida_confirmed(partida_id:int) -> void:
	cargando_panel.visible = true
	GameManager.cargar(partida_id)
	SceneManager.cambiar_escena(mundo_scena)
	yield(SceneManager,"fade_in_completed")
	cargando_panel.visible = false
	GameDefinitions.on_gui = false
	queue_free()


