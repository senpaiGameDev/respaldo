extends Control


func _ready() -> void:
    GameDefinitions.on_gui = true

func _process(delta: float) -> void:
    if Input.is_action_just_pressed("ui_select"):
        _salir()

func _salir() -> void:
    GameDefinitions.on_gui = false
    queue_free()
    get_tree().paused = false

