extends Object

enum Material {HIERRO,DIAMANTE,MADERA,OBSIDIANA,DEMONIUM}

static func get_armas() -> Dictionary:
    return {
        "Espada": {
            Material.MADERA: { 
                "fuerza": 10,
                "magia": 5
            },
            Material.HIERRO: { 
                "fuerza": 30,
                "magia": 15
            },
            Material.DIAMANTE: { 
                "fuerza": 100,
                "magia": 100
            },
            Material.OBSIDIANA: {
                "fuerza": 150,
                "magia": 50,
            },
            Material.DEMONIUM: { 
                "fuerza": 250,
                "magia": 300
            }
        },
        "Daga": {
            Material.MADERA: {
                "fuerza": 40,
                "magia": 5
            },
            Material.HIERRO: {
                "fuerza": 50,
                "magia": 30
            },
            Material.DIAMANTE:{
                "fuerza": 100,
                "magia": 50
            },
            Material.OBSIDIANA:{
                "fuerza": 200,
                "magia": 60
            },
            Material.DEMONIUM: {
                "fuerza": 350,
                "magia": 80
            }
        },
        "Hacha":{
            Material.MADERA: {
                "fuerza": 30,
                "magia": 0
            },
            Material.HIERRO:{
                "fuerza": 60,
                "magia": 10
            },
            Material.DIAMANTE: {
                "fuerza": 130,
                "magia": 13
            },
            Material.OBSIDIANA: {
                "fuerza": 300,
                "magia": 20
            },
            Material.DEMONIUM: {
                "fuerza": 500,
                "magia": 30
            }
        },
        "Vara": {
            Material.MADERA: {
                "fuerza": 10,
                "magia": 40
            },
            Material.HIERRO: {
                "fuerza": 15,
                "magia": 60
            },
            Material.DIAMANTE:{
                "fuerza": 30,
                "magia": 80
            },
            Material.OBSIDIANA:{
                "fuerza": 40,
                "magia": 110
            },
            Material.DEMONIUM:{
                "fuerza": 80,
                "magia": 350
            }
        },
        "Baston":{
            Material.MADERA: {
                "fuerza": 5,
                "magia": 50
            },
            Material.HIERRO: {
                "fuerza": 10,
                "magia": 75
            },
            Material.DIAMANTE:{
                "fuerza": 15,
                "magia": 120
            },
            Material.OBSIDIANA:{
                "fuerza": 20,
                "magia": 250
            },
            Material.DEMONIUM:{
                "fuerza": 40,
                "magia": 550
            }
        },
        "Wada": {
            Material.MADERA: {
                "fuerza": 50,
                "magia": 50
            },
            Material.HIERRO: {
                "fuerza": 75,
                "magia": 75
            },
            Material.DIAMANTE:{
                "fuerza": 100,
                "magia": 100
            },
            Material.OBSIDIANA:{
                "fuerza": 200,
                "magia": 200
            },
            Material.DEMONIUM:{
                "fuerza": 350,
                "magia": 350
            }
        }
    }

static func get_arma_stats(arma:String, material_arma:int)-> Dictionary:
    var armas = get_armas()
    if armas.has(arma):
        var mi_arma = armas.get(arma)
        if mi_arma.has(material_arma):
            return mi_arma.get(material_arma)
        else:
            Debugger.log("El arma %s no tiene la variación %s" % [arma , material_arma],"ArmaStats")
            return {}
    else:
        Debugger.log("No hay registros de estas del arma: %s" % arma,"ArmaStats")
        return {}