class_name Arma
extends Node2D

signal ataque_finalizado

const ANIMACION_ATACAR = "Atacar"
const ANIMACION_QUIETO = "Quieto"

const ArmasData = preload("res://sistema_de_combate/Armas/Data.gd")

export(int,
	"hierro",
	"diamante",
	"madera",
	"obsidiana", 
	"demonium"
	) var frame  = 0

export(String,
	"",
	"Espada",
	"Daga",
	"Hacha",
	"Vara",
	"Baston",
	"Wada") var tipo_arma = "Espada" 


var fuerza = 0
var magia = 0 

onready var sprite = get_node("Position2D/Sprite")
onready var animaciones = get_node("Animaciones")

func _ready() -> void:
	animaciones.connect("animation_finished",self,"_on_animaciones_animation_finished")
	sprite.frame = frame
	_cargar_datos(sprite.frame)

func _cargar_datos(nuevo_frame) -> void:
	var dic = ArmasData.get_arma_stats(tipo_arma,nuevo_frame)
	fuerza = dic["fuerza"]
	magia = dic["magia"]

func cambiar_material(material:int) -> void:
	#$if GameDefinitions.ARMAS_MATERIALES.has(str(material)):
	frame = material
	sprite.frame = material
	_cargar_datos(material)
	#else:
	#	Debugger.log("El material :%d no existe en GameDefinitions" %material,name)

func get_fuerza() -> int:
	var dic = ArmasData.get_arma_stats(tipo_arma,sprite.frame)
	Debugger.log("arma atributos: %s" %str(dic), name)
	return dic['fuerza']

func get_magia() -> int:
	var dic = ArmasData.get_arma_stats(tipo_arma,sprite.frame)
	Debugger.log("arma atributos: %s" %str(dic), name)
	return dic['magia']

func atacar() -> void:
	animaciones.play(ANIMACION_ATACAR)

func _on_animaciones_animation_finished(animacion_nombre:String) -> void:
	if animacion_nombre == ANIMACION_ATACAR:
		emit_signal("ataque_finalizado")
		animaciones.play(ANIMACION_QUIETO)