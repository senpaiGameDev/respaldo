class_name OleaadasGUI
extends Control

onready var progress_bar = get_node("HBoxContainer/ProgressBar")

func inicializar(num_oleadas:int) -> void:
	progress_bar.max_value = num_oleadas
	progress_bar.value = num_oleadas

func actualizar(nuevo_num_oleadas:int) -> void:
	progress_bar.value = nuevo_num_oleadas


