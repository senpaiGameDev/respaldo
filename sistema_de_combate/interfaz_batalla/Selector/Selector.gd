extends Node2D
# Esta clase Selector se encarga de mostrar un apuntador el cual le permite al usuario
# Seleccionar al objetivo de su comando. El selecotor depende de que los actores esten 
# acomodados en un tablero, ya que los movimientos del selector solo seran entre las
# posiciones de dicho tablero. Este selector puede ser manejado por el usuario
# con las teclas de flecha  y por el mouse. 
# una vez seleccionado el objetivo mediante un click o la tecla "Enter" este objetivo se manda
# mediante una señal para que sea tratado por el nodo encargado.

signal mandar_objetivo(objetivo)
signal cancelar

onready var tween = get_node("Tween")
onready var animacion = get_node("Animacion")
onready var textura = get_node("Textura")
const DURACION_MOVIMIENTO = 0.2

var objetivo:Actor = null 
var mi_tablero:Array = []
var eje_x = 0
var eje_y = 0
var max_x = 0
var max_y = 0
var jugador_en_turno:Jugador = null
var inicializado = false

var activo = false

func _ready() -> void:
	tween.connect("tween_completed",self,"_on_tween_completed")
	hide()
	salir()

func _process(_delta: float) -> void:
	if is_instance_valid(jugador_en_turno):
		if not jugador_en_turno.vivo:
			cancelar()


func inicializar(tablero:Tablero) -> void:
	if tablero.tablero.empty():
		return
	eje_x = 0
	eje_y = 0
	mi_tablero = tablero.tablero
	max_x = mi_tablero.size()
	mover_a(mi_tablero[eje_x][eje_y])
	inicializado = true

#cuando se activa el selector todos los actores pasan a
#ser seleccionables
func entrar(jugador:Jugador) -> void:
	GameDefinitions.on_gui = true
	jugador_en_turno = jugador
	if not jugador_en_turno.is_connected("jugador_muerto",self,"_on_jugador_muerto"):
		jugador_en_turno.connect("jugador_muerto",self,"_on_jugador_muerto")
	get_tree().call_group("Actor", "es_seleccionable")
	eje_x = 0
	eje_y = 0
	mover_a(mi_tablero[eje_x][eje_y])
	#_apuntar_enemigo()

func _on_jugador_muerto() -> void:
	salir()

func _input(evento: InputEvent) -> void:
	if not inicializado: return
	get_viewport().unhandled_input(evento) #hack para que el input se propague y el actor detecte el mouse en su area
	if evento is InputEventMouseButton and evento.button_index == BUTTON_LEFT:
		buscar_objetivo_x_apuntador()	
		return
	if evento.is_action_pressed("ui_cancel"):
		cancelar()
	if evento.is_action_pressed("ui_accept"):
		seleccionar()
	if evento.is_action_pressed("ui_up"):
		buscar_objetivo(Vector2.UP)
	elif evento.is_action_pressed("ui_down"):
		buscar_objetivo(Vector2.DOWN)
	elif evento.is_action_pressed("ui_left"):
		buscar_objetivo(Vector2.LEFT)
	elif evento.is_action("ui_right"):
		buscar_objetivo(Vector2.RIGHT)


func _apuntar_enemigo() -> void:
	if mi_tablero[eje_x][eje_y] is Base:
		return
	for i in range(mi_tablero.size()):
		for actor in mi_tablero[i]:
			if actor is Base:
				mover_a(actor)


# Si el usuario se mueve a la izquierda o derecha, entonces el valor de max_y
# tiene que volver a calcularse ya que nos estamos moviendo a un nuevo arreglo donde
# el max_y es el número de elementos que tiene ese arreglo. 
# si nos movemos de arriba  o abajo ya tenemos calculado el max_y por que el eje_x sigue
# siendo el mismo.
func buscar_objetivo(direccion:Vector2) -> void:
	if tween.is_active():
		return
	max_y = mi_tablero[eje_x].size()
	match direccion:
		Vector2.UP:
			eje_y -= 1
			eje_y = eje_y if eje_y >= 0 else max_y - 1
			mover_a(mi_tablero[eje_x][eje_y])
		Vector2.DOWN:
			eje_y += 1
			eje_y = eje_y if eje_y < max_y else 0 
			mover_a(mi_tablero[eje_x][eje_y])
		Vector2.LEFT:
			eje_x -= 1
			eje_x = eje_x if eje_x >= 0 else max_x - 1
			max_y = mi_tablero[eje_x].size()
			eje_y = eje_y if eje_y < max_y else max_y -1
			mover_a(mi_tablero[eje_x][eje_y])
		Vector2.RIGHT:
			eje_x += 1
			eje_x = eje_x if eje_x < max_x else 0
			max_y = mi_tablero[eje_x].size()
			eje_y = eje_y if eje_y < max_y else max_y - 1
			mover_a(mi_tablero[eje_x][eje_y])
			
func buscar_objetivo_x_apuntador() -> void:
	for cola in mi_tablero:
		for actor in cola:
			#pass
			if actor.seleccionado:
				if global_position == actor.selector.global_position:
					seleccionar()
					return
				else:
					mover_a(actor)

func mover_a(nuevo_objetivo:Actor) -> void:
	if not is_instance_valid(nuevo_objetivo): return
	objetivo = nuevo_objetivo
	var posicion_selector = objetivo.selector.global_position
	if objetivo.is_in_group("Jugador"):
		textura.flip_h = true
	else:
		textura.flip_h = false
	animacion_mover_a(posicion_selector)

func animacion_mover_a(posicion:Vector2) -> void:
	animacion.stop()
	tween.interpolate_property(
		self,
		"global_position",
		global_position,
		posicion,
		DURACION_MOVIMIENTO,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN
	)
	tween.start()

#cuando se sale del selector, todos los actores existentes dejan de ser 
#seleccionables 
func salir() -> void:
	GameDefinitions.on_gui = false
	get_tree().call_group("Actor", "no_es_seleccionable")

func seleccionar() -> void:
	if is_instance_valid(objetivo):
		emit_signal("mandar_objetivo",objetivo)
		salir()
	else:
		cancelar()

func _on_tween_completed(_objeto:Object,propiedad:NodePath) -> void:
	if propiedad == ":global_position":
		animacion.play("Parpadear")

func cancelar() -> void:
	emit_signal("cancelar")
	salir()
