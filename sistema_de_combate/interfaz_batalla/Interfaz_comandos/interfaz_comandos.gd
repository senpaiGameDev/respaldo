extends Control
#Esta clase se encarga de mostrar al usuario la interfaz en la cual puede seleccionar un comando
#a realizar, a parte de mostrar información del actor que esta en turno y por lo tanto el actor
#que ejecutara dicho comando. 
# Se planea implementar una sub interfaz la cual surga cuando el usuario ejecute el comando
# mágia. 
# A parde de Hacer los comandos globales para que  no existan incongruencias entre lo que manda
# la interfaz y lo que recibe el actor.

signal mandar_accion(comando)
export(PackedScene) var InterfazMagia
export(PackedScene) var InterfazArticulo

onready var icono = get_node("Contenedor/PersonajeInfo/Icono")
onready var personaje_nombre = get_node("Contenedor/PersonajeInfo/Nombre")
onready var comandos = get_node("Contenedor/Comandos")
onready var contenedor = get_node("Contenedor")

#const InterfazMagia = preload("res://sistema_de_combate/interfaz_batalla/Interfaz_comandos/Interfaz_magia/Interfaz_magia.tscn")

var mi_jugador:Jugador = null
var sub_interfaz = null

func _ready() -> void:
	conectar_comandos()
	hide()

func _process(delta: float) -> void:
	if is_instance_valid(mi_jugador):
		if not mi_jugador.vivo:
			_mandar_comando_vacio()

func asignar_datos(jugador:Jugador) -> void:
	mi_jugador = jugador

	if not mi_jugador.is_connected("cambio_mp",self,"_on_jugador_cambio_mp"):
		mi_jugador.connect("cambio_mp",self,"_on_jugador_cambio_mp")

	var datos = jugador.obtener_datos()
	if not datos['error']:
		icono.texture = load(datos['icono'])
		personaje_nombre.text = datos['nombre']
	cambiar_estado_boton('Magia',jugador.sin_magia)

func _on_jugador_cambio_mp(mp:int) -> void:
	cambiar_estado_boton("Magia",mp <= 0)

#Al entrar a la interfaz existe un problema y es que el selector al manejar la entrada "Enter"
#No la maneja del todo y se sigue propagando por los diferentes nodos que existen.
# por lo tanto cuando se entra a la interfaz esta maneja ese enter y manda el comando enfocado
# el cual es el primero en la lista de comandos. Para evitar esto
# al entrar en la interfaz de comandos se ejecuta la función "accept_event()" la cual indica que los
# eventos ya han sido manejados haciendo que estos se dejen de propagar.
func entrar(jugador:Jugador) -> void:
	GameDefinitions.on_gui = true
	accept_event()
	if not jugador.vivo:
		_mandar_comando_vacio()
		return
	asignar_datos(jugador)
	enfocar_comandos()
	if sub_interfaz != null:
		sub_interfaz.entrar(jugador)


func _mandar_comando_vacio() -> void:
	_salir("")

func enfocar_comandos() -> void:
	comandos.get_child(0).grab_focus()

func conectar_comandos() -> void:
	for comando in comandos.get_children():
		comando.connect("pressed",self,"_on_comando_pressed",[comando.comando_key])

func _on_comando_pressed(comando:String) -> void:
	if comando == GameDefinitions.Comando_magia:
		lanzar_interfaz_magia()
	elif comando == GameDefinitions.Comando_usar_objeto:
		lanzar_interfaz_articulo()
	else:
		_salir(comando)

#deshabilitar el botón de magia
func cambiar_estado_boton(boton_buscado:String,estado:bool) -> void:
	for boton in comandos.get_children():
		if boton_buscado == boton.name:
			boton.disabled = estado

func lanzar_interfaz_articulo() -> void:
	hide()
	var interfaz_articulo = InterfazArticulo.instance()
	interfaz_articulo.connect("articulo_seleccionado", self, "_on_interfazArticulo_articulo_seleccionado")
	interfaz_articulo.connect("listo",self,"_on_interfazArticulo_listo")
	get_parent().add_child(interfaz_articulo)

func lanzar_interfaz_magia() -> void:
	hide()
	var interfaz_magia = InterfazMagia.instance()
	interfaz_magia.connect("magia_seleccionada",self,"_on_interfazMagia_magia_seleccionada")
	interfaz_magia.connect("listo",self,"_on_interfazMagia_listo")
	get_parent().add_child(interfaz_magia)

func _on_interfazArticulo_listo(interfaz:Control) -> void:
	sub_interfaz = interfaz
	interfaz.entrar(mi_jugador)

func _on_interfazMagia_listo(interfaz:Control) -> void:
	sub_interfaz = interfaz
	interfaz.entrar(mi_jugador)

func _on_interfazArticulo_articulo_seleccionado(articulo_id:String, interfaz:Control) -> void:
	var padre = get_parent()
	padre.remove_child(interfaz)
	interfaz.queue_free()
	show()
	sub_interfaz = null
	if articulo_id != "":
		_salir(articulo_id)
	else:
		enfocar_comandos()


func _on_interfazMagia_magia_seleccionada(magia:String,interfaz:Control) -> void:
	var padre = get_parent()
	padre.remove_child(interfaz)
	interfaz.queue_free()
	show()
	sub_interfaz = null
	if magia != '':
		_salir(magia)
	else:
		enfocar_comandos()

func _salir(accion: String) -> void:
	emit_signal("mandar_accion",accion)
	GameDefinitions.on_gui = false
