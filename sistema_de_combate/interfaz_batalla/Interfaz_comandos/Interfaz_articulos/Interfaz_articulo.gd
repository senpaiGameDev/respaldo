extends Control

signal articulo_seleccionado(articulo_id,interfaz)
signal listo

export(PackedScene) var btnArticulo_ref = null

onready var articulos_contenedor = get_node("Contenedor/ScrollContainer/Articulos_contenedor")
onready var sin_articulos = get_node("Contenedor/SinArticulos")
var btns_articulos = []
var actor:Actor

func _ready() -> void:
	_cargar_articulos()
	GameManager.partida.inventario.connect("cambio_inventario_articulos", self, "_on_inventario_inventario_cambio")
	emit_signal("listo",self)

func _recargar_inventario() -> void:
	_limpiar_articulos()
	_cargar_articulos()

func _limpiar_articulos() -> void:
	for articulo in articulos_contenedor.get_children():
		articulos_contenedor.remove_child(articulo)
		articulo.queue_free()

func _on_inventario_inventario_cambio(articulo_id:int, cantidad:int) -> void:
	for btn_articulo in get_children():
		if btn_articulo.articulo_id == articulo_id:
			btn_articulo.actualizar(cantidad)
	if cantidad <= 0:
		_recargar_inventario()

func _cargar_articulos() -> void:
	if GameManager.partida.inventario.get_articulos().empty():
		sin_articulos.visible = true
	else:
		sin_articulos.visible = false

	for articulo_id in GameManager.partida.inventario.get_articulos().keys():
		if GameManager.partida.inventario.se_tiene_articulo(articulo_id):
			var articulo = ItemDefinitions.get_articulo_data_by_id(articulo_id)
			_instanciar_boton(articulo)

func _instanciar_boton(articulo_data) -> void:
	var btn = btnArticulo_ref.instance()
	btn.connect("boton_presionado", self, "_on_btn_articulo_mandar_articulo")
	articulos_contenedor.add_child(btn)
	var cantidad = GameManager.partida.inventario.obtener_cantidad_articulo(articulo_data.id)
	btn.inicializar(articulo_data.nombre,articulo_data.id, articulo_data.textura_path,cantidad)


func _input(evento: InputEvent) -> void:
	if evento.is_action_pressed("ui_cancel"):
		emit_signal("articulo_seleccionado","",self)

#cuando entra el jugador ya tiene asignadas las magias que puede
#lanzar de acuerdo con la cantidad de mp que tiene.
#En caso de que el primer botón no estuviera habilitado 
#se queda habilitado el ultimo que se habilito
func entrar(jugador: Jugador) -> void:
	accept_event()
	if not jugador.is_connected("jugador_muerto",self,"_on_actor_muerto"):
		jugador.connect("jugador_muerto",self, "_on_actor_muerto")
	var articulos_cargados = articulos_contenedor.get_children()
	if len(articulos_cargados) > 0:
		articulos_cargados[0].enfocar()
		
func _on_btn_articulo_mandar_articulo(id:int) -> void:
	emit_signal("articulo_seleccionado",str(id),self) #pasar el articulo que se utilizará.

func _on_actor_muerto() -> void:
	emit_signal("articulo_seleccionado","",self)


