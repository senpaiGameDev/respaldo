class_name BtnArticulo
extends HBoxContainer

signal boton_presionado(articulo_id)

onready var boton = get_node("Boton")
onready var textura = get_node("Textura")


var articulo_id
var articulo_nombre

func _ready() -> void:
	boton.connect("pressed",self,"_on_boton_pressed")

func inicializar(nombre:String, id:int, textura_path:String, cantidad:int):
	boton.text = nombre + " x " + str(cantidad)
	textura.texture = load(textura_path)
	articulo_id = id
	articulo_nombre = nombre

func actualizar(cantidad:int) -> void:
	boton.text = articulo_nombre + " x " + str(cantidad)

func enfocar() -> void:
	boton.grab_focus()

func _on_boton_pressed() -> void:
	emit_signal("boton_presionado",articulo_id)



