tool
extends Button

export(String,"", #Error de Godot, Nunca toma en cuenta al primer elemento u.u
	"magia", #GameDefinitions.Comando_magia
	"pasar", #GameDefinitions.Comando_pasar
	"usar_objeto", #GameDefinitions.Comando_usar_objeto
	"atacar", #GameDefinitions.Comando_atacar
	"habilidad" #GameDefinitions.Comando_habilidad
	) var comando_key

var error_msg = "Error debes de asignar la variable 'comando_key' en todos los comandos"

func _ready() -> void:
	assert(comando_key != "",error_msg)
