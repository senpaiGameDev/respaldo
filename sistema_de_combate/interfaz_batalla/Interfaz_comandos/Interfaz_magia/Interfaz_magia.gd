extends Control

signal magia_seleccionada(magia,interfaz)
signal listo

onready var botones = get_node("Contenedor/Magias")
onready var sinMpLabel = get_node("SinMp")

var magias_interfaz
var mi_jugador = null

func _ready() -> void:
	magias_interfaz = botones.get_children()
	for btn_magia in magias_interfaz:
		btn_magia.connect("boton_presionado",self,"_on_btn_magia_mandar_magia")

	emit_signal("listo",self)

func _input(evento: InputEvent) -> void:
	if evento.is_action_pressed("ui_cancel"):
		emit_signal("magia_seleccionada","",self)

#cuando entra el jugador ya tiene asignadas las magias que puede
#lanzar de acuerdo con la cantidad de mp que tiene.
#En caso de que el primer botón no estuviera habilitado 
#se queda habilitado el ultimo que se habilito
func entrar(jugador: Jugador) -> void:
	accept_event()
	mi_jugador = jugador
	if not jugador.is_connected("jugador_muerto",self,"_on_jugador_muerto"):
		jugador.connect("jugador_muerto",self, "_on_jugador_muerto")

	if not jugador.is_connected("cambio_mp",self,"_on_jugador_cambio_mp"):
		jugador.connect("cambio_mp",self,"_on_jugador_cambio_mp")

	if jugador.sin_magia:
		sinMpLabel.visible = true
	else:
		sinMpLabel.visible = false
		_actualizar_botones(jugador)

func _actualizar_botones(jugador: Jugador) -> void:
	var contador_magias = 0
	for boton in magias_interfaz:
		if jugador.magia_disponible(boton.magia):
			boton.visible = true
			contador_magias += 1
		else:
			boton.visible = false

	for boton in magias_interfaz:
		if boton.visible:
			boton.enfocar()
			return

	if contador_magias == 0:
		sinMpLabel.visible = true

func  _on_jugador_cambio_mp(_mp:int) -> void:
	if mi_jugador != null and is_instance_valid(mi_jugador):
		_actualizar_botones(mi_jugador)


func _on_btn_magia_mandar_magia(magia:String) -> void:
	emit_signal("magia_seleccionada",magia,self)

func _on_jugador_muerto() -> void:
	emit_signal("magia_seleccionada","",self)
