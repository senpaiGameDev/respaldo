class_name BtnMagia
extends HBoxContainer

signal boton_presionado(magia)

export(String,"", #Error de Godot, Nunca toma en cuenta al primer elemento
	"Fulgur", #MagicDefinitions.fulgur_key
	"Sana", #MagicDefinitions.sana_key
	"Ignis", #MagicDefinitions.ignis_key
	"Aqua" #MagicDefinitions.aqua_key
	) var magia

onready var boton = get_node("Boton")
onready var textura = get_node("Textura")

var msg_error = "ERROR: Debes de asignar la variable 'magia' en todos los botones de magia"

func _ready() -> void:
	assert(magia != "",msg_error)
	var magias = [magia]
	MagicDefinitions.assert_magias_validas(magias)
	config_boton()

func config_boton():
	boton.connect("pressed",self,"_on_boton_pressed")
	boton.text = magia

func enfocar() -> void:
	boton.grab_focus()

func _on_boton_pressed() -> void:
	emit_signal("boton_presionado",magia)
