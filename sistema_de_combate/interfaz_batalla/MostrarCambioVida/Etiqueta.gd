extends Label

signal animacion_terminada(etiqueta)

onready var tween = get_node("Tween")

const DURACION_ANIMACION = 0.6


func _ready() -> void:
	tween.connect("tween_completed",self,"_on_tween_tween_completed")


func configurar(cadena:String,color:Color) -> void:
	text = cadena
	modulate = color

func animacion() -> void:
	tween.interpolate_property(
		self,
		"modulate",
		modulate,
		Color("#00ffffff"),
		DURACION_ANIMACION,
		Tween.TRANS_LINEAR,
		Tween.EASE_OUT_IN
	)
	tween.start()

func _on_tween_tween_completed(objeto:Object,propiedad:NodePath) -> void:
	if propiedad == ":modulate" and objeto == self:
		emit_signal("animacion_terminada",self)
