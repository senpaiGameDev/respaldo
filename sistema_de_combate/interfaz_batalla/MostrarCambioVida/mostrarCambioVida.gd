class_name MostrarCambiosVida
extends Control

signal termino(mostrador)

onready var contenedor = get_node("Contenedor")

const ETIQUETA = preload("res://sistema_de_combate/interfaz_batalla/MostrarCambioVida/Etiqueta.tscn")

func posicionar_etiqueta() -> void:
	contenedor.rect_position = rect_position

func mostrar_cambio(vida:int, signo:int) -> void:
	var etiqueta:Label = crear_etiqueta(vida,signo)
	etiqueta.connect("animacion_terminada",self,"_on_etiqueta_animacion_terminada")
	contenedor.add_child(etiqueta)
	etiqueta.animacion()


func crear_etiqueta(vida:int,signo:int) -> Label:
	var cadena = '+' if signo > 0 else '-'
	var color_texto = Color.green if signo > 0 else Color.red
	var etiqueta:Label = ETIQUETA.instance()
	cadena += str(abs(vida))
	etiqueta.configurar(cadena,color_texto)
	return etiqueta

func _on_etiqueta_animacion_terminada(etiqueta:Label) -> void:
	contenedor.remove_child(etiqueta)
	if contenedor.get_children().empty():
		emit_signal("termino",self)

