extends VBoxContainer

export(int,0,3) var max_integrantes = 0 

const InterfazIndividual = preload("res://sistema_de_combate/interfaz_batalla/interfaz_grupal/MostradorEstadisticas.tscn")

func agregar_nuevo_integrante(jugador:Jugador) -> void:
	var nuevo_integrante = InterfazIndividual.instance()
	add_child(nuevo_integrante)
	nuevo_integrante.asignar_jugador(jugador)
