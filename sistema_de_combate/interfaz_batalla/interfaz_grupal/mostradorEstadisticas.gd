extends HBoxContainer

onready var barraVida = get_node("PuntosVida")
onready var barraMp = get_node("PuntosMagia")
onready var barraTiempo = get_node("TiempoEspera")
onready var icono = get_node("Icono")

var jugador

func asignar_jugador(jugador_asignado:Jugador):
	jugador = jugador_asignado
	jugador.connect("avanzo_tiempo",self,"_on_jugador_avanzo_tiempo")
	jugador.connect("cambio_mp",self,"_on_jugador_cambio_mp")
	jugador.connect("cambio_vida",self,"_on_jugador_cambio_vida")
	inicializar_barras() 
	asignar_icono()

func asignar_icono() -> void:
	var datos = jugador.obtener_datos()
	if not datos['error']:
		icono.texture = load(datos['icono'])

func inicializar_barras() -> void:
	barraVida.inicializar(jugador.vida_max)
	barraMp.inicializar(jugador.mp_max)
	barraTiempo.inicializar(jugador.tiempo_espera * 100)

func _on_jugador_avanzo_tiempo(tiempo_faltante:float) -> void:
	var tiempo = jugador.tiempo_espera - tiempo_faltante if jugador.tiempo_espera != tiempo_faltante else jugador.tiempo_espera
	barraTiempo.cambio_valor(tiempo * 100)

func _on_jugador_cambio_mp(mp:int) -> void:
	barraMp.cambio_valor(mp)

func _on_jugador_cambio_vida(vida:int) -> void:
	barraVida.cambio_valor(vida)
