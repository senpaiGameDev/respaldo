extends HBoxContainer

export(Color) var color_barra_llena = Color.white
export(Color) var color_barra = Color.white


onready var barraProgreso = get_node("Progreso/ProgressBar")
onready var etiqueta = get_node("Progreso/Etiqueta")

var valor_max = 0.0
var valor_actual = 0.0

func _ready() -> void:
	barraProgreso.tint_over = color_barra_llena
	barraProgreso.tint_progress = color_barra

func inicializar(maximo:float):
	valor_actual = maximo
	valor_max = maximo
	barraProgreso.max_value = valor_actual
	actualizar()

func actualizar():
	barraProgreso.value = valor_actual
	etiqueta.text = str(valor_actual) + "/" + str(valor_max)

func cambio_valor(valor:float):
	valor_actual = valor
	actualizar()
