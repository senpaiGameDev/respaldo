class_name Jugador
extends Actor

signal cambio_vida(vida)
signal cambio_mp(mp)
signal avanzo_tiempo(tiempo)
signal jugador_muerto 
signal jugador_revido
signal subio_nivel(nombre,nivel)

const DISTANCIA_ACCIONAR:int = 50
const DURACION_ACCIONAR:float = 1.0
const ANIMACION_CAMINAR:String = "Caminar"
const DURACION_TAMBALEAR:float = 0.3

export(String,FILE) var icono 
export(String) var nombre = ''

onready var sprite_muerto = get_node("DeadSprite")
onready var tween = get_node("Tween")
onready var arma = get_node("Arma").get_child(0)
onready var contenedor_arma = get_node("Arma")
onready var animaciones = get_node("Animaciones")
onready var sprite  = get_node("Sprite")
onready var timer = get_node("Timer")
onready var nodo_magia:Magia = get_node("Magia")
onready var hurt_audio = get_node("HurtAudio")

var posicion_original:Vector2
var objetivo_actual:Actor = null

func _ready() -> void:
	posicion_original = position
	asignar_desde_estadistica()
	timer.wait_time = tiempo_espera
	tween.connect("tween_completed",self,"_on_tween_tween_completed")
	timer.connect("timeout",self,"_on_timer_timeout")
	if arma:
		arma.connect("ataque_finalizado",self,"_on_arma_ataque_finalizado")
	nodo_magia.load_gasto_x_magia_dicc()
	nodo_magia.load_animaciones_dicc()
	set_arma()

func _process(_delta: float) -> void:
	if not timer.is_stopped():
		emit_signal("avanzo_tiempo",timer.time_left)

func set_arma() -> void:
	if arma != null:
		var arma_material = GameManager.partida.configuracion_jugadores.get_arma_jugador(
			estadisticas.personaje_id)
		arma.cambiar_material(arma_material)

#esta funcion la utiliza como api El "mostradorEstadisticas.gd"
#por lo que debes de tener cuidado por si vas a modificar
#algo dentro,recuerdes que va a afectar a la interfaz mencionada
func asignar_desde_estadistica() -> void:
	tiempo_espera = estadisticas.get_tiempo()
	vida_max = estadisticas.hp_max
	mp_max = estadisticas.mp_max
	fuerza = estadisticas.fuerza
	poder_magico = estadisticas.poder_magico
	resistencia = estadisticas.resistencia
	resistencia_magica = estadisticas.resistencia_magica
	experiencia = estadisticas.experiencia
	nivel = estadisticas.nivel

#se manda la señal de turno finalizado cuando el comando se termine de ejecutar
#para evitar que se ejecute más de un comando sobre el mismo objetivo
#al mismo tiempo
func ejecutar_accion(accion:String,objetivo:Actor) -> void:
	emit_signal("empezar_accion",self)
	puede_estar_en_turno = false
	objetivo_actual = objetivo
	if accion == GameDefinitions.Accion_atacar:
		atacar()
	elif accion in GameDefinitions.Acciones_magia:
		lanzar_hechizo(accion)
	elif accion in GameDefinitions.Acciones_articulo:
		usar_articulo(accion)
	else:
		accion_terminada()
		print("comando sin implementar")

	terminar_turno()

func usar_articulo(articulo_id:String) -> void:
	if is_instance_valid(objetivo_actual):
		var articulo = ItemDefinitions.get_articulo_data_by_id(int(articulo_id))
		if is_instance_valid(articulo):
			add_child(articulo.articulo_obj)
			articulo.articulo_obj.utilizar(objetivo_actual)
			yield(articulo.articulo_obj,"proceso_de_articulo_terminado")

			remove_child(articulo.articulo_obj)
			_animacion_finalizar_accion()
	else:
		accion_terminada()
	
func atacar() -> void: 
	contenedor_arma.visible = true
	if is_instance_valid(objetivo_actual):
		_animacion_atacar(objetivo_actual.global_position)

func lanzar_hechizo(magia:String) -> void:
	if is_instance_valid(objetivo_actual):
		_animacion_inicio_accion()
		nodo_magia.ejecutar(objetivo_actual,magia)
		yield(nodo_magia,"proceso_de_magia_terminado")
		_animacion_finalizar_accion()
	else:
		accion_terminada()

func obtener_datos() -> Dictionary:
	var error = existe_info()
	return {
		"icono": icono,
		"nombre": nombre,
		"error": error
	}

func existe_info() -> String:
	var error = ""
	if not nombre:
		error += "\tEl jugador no tiene nombre"
	if not icono:
		error += "\tEl jugador: " + name + "No tiene asignado un icono"
	return error

func _on_arma_ataque_finalizado() -> void:
	if is_instance_valid(objetivo_actual):
		objetivo_actual.recibir_agravio(calcular_fuerza_total(objetivo_actual))
	_animacion_finalizar_accion()
	contenedor_arma.visible = false

func _animacion_atacar(objetivo_position:Vector2) -> void:
		#animación de caminar hacia enfrente
		var offset = 80
		var posicion_final_x = objetivo_position.x + offset
		var posicion_final = Vector2(posicion_final_x, objetivo_position.y)
		tween.interpolate_property(
			self,
			'global_position',
			global_position,
			posicion_final, 
			DURACION_ACCIONAR/2,
			Tween.TRANS_LINEAR,
			Tween.EASE_IN
		)
		tween.start()
		animaciones.stop()

func _animacion_inicio_accion() -> void:
	#direccion en x del objetivo:
	var direccion = Vector2.LEFT 
	#animación de caminar hacia enfrente
	tween.interpolate_property(
		self,
		'position',
		position,
		position + (DISTANCIA_ACCIONAR * direccion), 
		DURACION_ACCIONAR/2,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN
	)
	tween.start()
	animaciones.stop()

func _animacion_finalizar_accion() -> void:
	sprite.flip_h = not sprite.flip_h
	animaciones.play(ANIMACION_CAMINAR)
	tween.interpolate_property(
		self,
		"position",
		position,
		posicion_original,
		DURACION_ACCIONAR/2,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN
	)
	tween.start()
	
func _on_tween_tween_completed(_objeto:Object,propiedad:NodePath) -> void:
	#utilizamos la posicion_original para diferenciar las animaciones de 
	#inciar acción y finalizar acción, ya que ambas utilizan la misma propiedad
	if propiedad == ":global_position":
		if arma and is_instance_valid(objetivo_actual):
			arma.atacar()
	if propiedad == ':position':
		if position == posicion_original:
			sprite.flip_h = not sprite.flip_h
			accion_terminada()

func _animacion_recibir_agravio() -> void:
	if vivo:
		hurt_audio.play()
	tween.interpolate_method(
		self,
		"animacion_parpadea_de_rojo",
		0,
		6,
		DURACION_TAMBALEAR,
		Tween.TRANS_ELASTIC,
		Tween.EASE_IN_OUT
	)
	tween.start()

func _on_subio_nivel(nivel:int) -> void:
	emit_signal("subio_nivel",nombre,nivel)

func _on_estadisticas_cambio_vida(nueva_vida:int,cantidad:int) -> void:
	emit_signal("cambio_vida",nueva_vida)
	if cantidad < 0:
		_animacion_recibir_agravio()
	if nueva_vida <= 0:
		_morir()

func _on_mp_cambio(mp:int) -> void:
	sin_magia = mp <= 0
	emit_signal("cambio_mp",mp)

func _on_timer_timeout() -> void:
	emit_signal("tiempo_espera_terminado")
	emit_signal("avanzo_tiempo",tiempo_espera)

func animacion_parpadea_de_rojo(progreso:float) -> void:
	var color = Color(1,1,1,1) if int(round(progreso)) % 2 == 0 else Color.red
	if round(progreso) == 0:
		sprite.scale = Vector2(0.86,1.03)
	elif round(progreso) == 6:
		sprite.scale = Vector2(1,1)
	sprite.modulate =  color

func calcular_fuerza_total(objetivo:Actor) -> int:
	var n_fuerza = fuerza
	if arma != null:
		n_fuerza += arma.get_fuerza()
		if is_instance_valid(objetivo):
			n_fuerza = (n_fuerza * 2) - (objetivo.resistencia * 0.5)
		n_fuerza = clamp(n_fuerza,0,n_fuerza)
	return n_fuerza

func terminar_turno() -> void:
	timer.start()
	emit_signal("turno_finalizado",self)

func accion_terminada() -> void:
	emit_signal("comando_ejecutado")
	puede_estar_en_turno = true
#función utilizada por interfaz_magia.gd para activar y desactivar magias
func magia_disponible(magia_key:String) -> bool:
	return nodo_magia.magia_disponible(magia_key)

func _morir() -> void:
	vivo = false
	sprite.visible = false
	sprite_muerto.visible = true
	#animaciones.stop()
	puede_estar_en_turno = false
	accion_terminada()
	terminar_turno()
	emit_signal("jugador_muerto")
	timer.stop()

func revivir() -> void:
	vivo = true
	recibir_agravio(vida_max / 3*-1)
	sprite_muerto.visible = false
	sprite.visible = true
	#animaciones.play()
	puede_estar_en_turno = true
	emit_signal("jugador_revido")
	timer.start()

