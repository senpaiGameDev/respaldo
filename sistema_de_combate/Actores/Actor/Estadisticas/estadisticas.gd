extends Node
#Este archivo va a  estar relacionado muy en profundidad con la base de datos
#porque tanto los niveles como los atributos abajo listados se guardaran en una 
#base de datos y se cargaran al momento de que este script este listo(_ready())
#todos los atributos export y el arreglo magias se deberan cargar de una base
#de datos y no aparecer como export, esto se hace inicialmente para hacer pruebas,
#hasta que se diseñe e implemente la base de datos
#algunos métodos relacionados con la carga estan con implementaciones que sirven solo
#para hacer pruebas.

signal cambio_vida(vida_actual,cantidad_quitada)
signal cambio_mp(mp_actual)
signal subio_nivel(nivel)
signal exp_incrementada

enum PERSONAJES_IDs {CABALLERO = 0, GUERRERO = 1, MAGA_BLANCA = 2, MAGA_OSCURA = 3, NINJA = 4, ENEMIGO = -1}
export(PERSONAJES_IDs) var personaje_id = PERSONAJES_IDs.ENEMIGO

const EstadisticasRaw = preload("res://Data/EstadisticasRaw.gd")

export(int) var hp_max:int = 100 
export(int) var mp_max:int = 20 
export(int) var fuerza:int = 10
export(int) var poder_magico:int = 20
export(int) var resistencia:int = 20
export(int) var resistencia_magica:int = 20
export(int) var experiencia:int = 1
export(int) var nivel:int = 1
export(float) var tiempo_espera = 3.0 setget set_tiempo, get_tiempo

export(int,25,80) var hp_level_up:int = 100
export(int,1,5) var mp_level_up:int = 10
export(int,1,3) var fuerza_level_up:int = 1
export(int,1,3) var poder_magico_level_up:int = 1
export(int,1,3) var resistencia_level_up:int = 1
export(int,1,3) var resistencia_magica_level_up:int = 1
export(float,0.01,0.015) var tiempo_de_espera_level_up:int = 0.01
#esta lista es utilizada por "interfaz_magia.gd" con ella
#se decide si habilitar o no los botones correspondientes 
#a cada magia.
var mp = 0 setget set_mp
var hp = 0 setget set_hp
var exp_siguiente_nivel = 0

func _ready():
	cargar_estadisticas()
	exp_siguiente_nivel = cal_exp_nivel(nivel)
	mp = mp_max
	hp = hp_max

func cargar_estadisticas() -> void:
	if personaje_id != PERSONAJES_IDs.ENEMIGO:
		var diccionario = GameManager.partida.configuracion_jugadores.get_stats_jugador(personaje_id)
		hp_max = diccionario["hp_max"]
		mp_max = diccionario["mp_max"]
		fuerza = diccionario["fuerza"]
		poder_magico = diccionario["poder_magico"]
		resistencia = diccionario["resistencia"]
		resistencia_magica = diccionario["resistencia_magica"]
		experiencia = diccionario["experiencia"]
		nivel = diccionario["nivel"]
		tiempo_espera = diccionario["tiempo_espera"]

func _guardar_exp() -> void:
	GameManager.partida.configuracion_jugadores.actualizar_exp_jugador(personaje_id,experiencia)

func _guardar_estadisticas() -> void:
	var estadisticas = EstadisticasRaw.new(
		hp_max,mp_max,fuerza,poder_magico,resistencia,resistencia_magica,experiencia,nivel,tiempo_espera)
	GameManager.partida.configuracion_jugadores.actualizar_stats_jugador(personaje_id,estadisticas.get_data())

func set_tiempo(tiempo:float) -> void:
	tiempo_espera = clamp(tiempo,GameDefinitions.Tiempo_espera_minimo,tiempo)

func get_tiempo() -> float:
	return tiempo_espera

func agregar_exp(xp:int) -> void:

	var ultimo_nivel = nivel
	experiencia += xp

	if experiencia >= exp_siguiente_nivel:
		_subir_de_nivel()

	if ultimo_nivel < nivel:
		_subir_atributos(nivel - ultimo_nivel)
		#_truncar_stats()
		_guardar_estadisticas()
		emit_signal("subio_nivel",nivel)

	_guardar_exp()
	emit_signal("exp_incrementada")

func _subir_de_nivel() -> void:
	nivel += 1
	exp_siguiente_nivel = cal_exp_nivel(nivel)
	if experiencia >= exp_siguiente_nivel:
		_subir_de_nivel()

func _subir_atributos(niveles_subidos:int) -> void:
	hp_max += hp_level_up * niveles_subidos
	mp_max += mp_level_up * niveles_subidos
	fuerza += fuerza_level_up * niveles_subidos
	poder_magico += poder_magico_level_up * niveles_subidos
	resistencia += resistencia_level_up * niveles_subidos
	resistencia_magica += resistencia_magica_level_up * niveles_subidos
	tiempo_espera -= tiempo_de_espera_level_up * niveles_subidos


func _truncar_stats() -> void:
	nivel = int(clamp(nivel, nivel, GameDefinitions.Ultimo_nivel))
	hp_max = int(clamp(hp_max,hp_max,GameDefinitions.Vida_limite))
	mp_max = int(clamp(mp_max,mp_max,GameDefinitions.Atributos_limite))
	fuerza = int(clamp(fuerza,fuerza,GameDefinitions.Atributos_limite))
	poder_magico = int(clamp(poder_magico,poder_magico,GameDefinitions.Atributos_limite))
	resistencia = int(clamp(resistencia, resistencia, GameDefinitions.Atributos_limite))
	resistencia_magica = int(clamp(resistencia_magica,resistencia_magica,GameDefinitions.Atributos_limite))

func cambiar_vida(impacto:int) -> void:
	self.hp += impacto
	emit_signal("cambio_vida",self.hp,impacto)

func cambiar_mp(cambio:int) -> void:
	if cambio < 0:
		self.mp -= abs(cambio)
	else:
		self.mp += abs(cambio)
	emit_signal("cambio_mp",self.mp)

func set_hp(monto:int) -> void:
	hp = clamp(monto,0,hp_max)

func set_mp(puntos:int) -> void:
	mp = clamp(puntos,0,mp_max)

func cal_exp_nivel(nivel_n:int) -> int:
	var siguiente_nivel = nivel_n + 1
	return int(round(( 4 * pow(siguiente_nivel, 3)) / 5))



