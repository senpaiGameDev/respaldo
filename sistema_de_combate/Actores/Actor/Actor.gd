class_name Actor
extends Position2D

#señal mandada cuando se empieza a ejecutar el comando recibido desde el usuario
signal turno_finalizado(actor)
signal actor_muerto(actor)
signal actor_resucitado(actor)
#esta señal se va a encargar de avisar que se empieza la acción para que ningun otro actor pueda ejecutar una acción
#hasta que se mande la señal de turno_finalizado
signal empezar_accion(actor)
#señal mandada cuando la acción del actor termina,(evitando con esto que el actor reciba
# otro comando cuando no ha terminado de ejecutar el primero)
signal comando_ejecutado
#Señal mandada cuando el tiempo de espera del actor termina.
#Las dos señales tanto comando_ejecutado y tiempo_espera_terminado son necesarias
#una es para evitar entrar en turno a un actor que no ha terminado de ejecutar su comando.
#y la otra para formar en la cola al actor hasta que termine su tiempo de espera.
signal tiempo_espera_terminado
signal exp_incrementada

onready var estadisticas:= get_node("Estadisticas")
onready var selector = get_node("Selector")
onready var areaSeleccionable  = get_node("AreaSeleccionable")
onready var posicionMostradorCambiosVida = get_node("PosicionMostradosCambiosVida")

const MostradorCambiosVida = preload("res://sistema_de_combate/interfaz_batalla/MostrarCambioVida/MostrarCambioVida.tscn")

var seleccionado = false
var puede_estar_en_turno = true
var esta_en_turno = false
var mostrador

var nivel
var fuerza_total
var fuerza
var vida_max 
var mp_max
var resistencia
var resistencia_magica
var poder_magico
var tiempo_espera
var experiencia
var sin_magia = false
var vivo = true

func _ready() -> void:
	areaSeleccionable.connect("mouse_entered",self,"_on_mouse_entered")
	areaSeleccionable.connect("mouse_exited",self,"_on_mouse_exited")
	estadisticas.connect("cambio_vida",self,"_on_estadisticas_cambio_vida")
	estadisticas.connect("cambio_mp",self,"_on_mp_cambio")
	estadisticas.connect("subio_nivel",self,"_on_subio_nivel")
	estadisticas.connect("exp_incrementada",self,"_on_exp_incrementada")
	no_es_seleccionable()

func asignar_estadisticas() -> void:
	pass

func agregar_exp(exp_n:int) -> void:
	estadisticas.agregar_exp(exp_n)

func _on_mouse_exited() -> void:
	seleccionado = false

func _on_mouse_entered() -> void:
	seleccionado = true

func es_seleccionable() -> void:
	areaSeleccionable.set_process_input(true)
	areaSeleccionable.input_pickable = true

func no_es_seleccionable() -> void:
	areaSeleccionable.input_pickable = false

func cambiar_poder_magico(gasto:int) -> void:
	if not vivo: 
		return
	estadisticas.cambiar_mp(gasto * -1)

func recibir_agravio(impacto:int) -> void:
	if not vivo: 
		return
	estadisticas.cambiar_vida(impacto*-1)
	var signo = 1 if impacto*(-1) > 0 else -1
	instanciar_mostrador(impacto,signo)

#cuando el actor esta muerto es necesario notificarlo mediante señales para que
# se actualize el tablero
func _on_estadisticas_cambio_vida(vida_actual:int,cantidad_quitada:int) -> void:
	if vida_actual <= 0:
		_morir()

func _morir() -> void:
	vivo = false
	emit_signal("actor_muerto",self)

func revivir() -> void:
	vivo = true
	emit_signal("actor_resucitado",self)

#se instancia un nodo del tipo control el cual se encarga de mostrar 
#la cantidad de daño recibido o la cantidad de vida curada.
func instanciar_mostrador(impacto:int,signo:int) -> void:
	if not is_instance_valid(mostrador):
		mostrador = MostradorCambiosVida.instance()
		posicionMostradorCambiosVida.add_child(mostrador)
		mostrador.rect_position = posicionMostradorCambiosVida.position
		mostrador.posicionar_etiqueta()
		mostrador.connect("termino",self,"_on_mostrador_termino")
	mostrador.mostrar_cambio(impacto,signo)

func _on_mostrador_termino(mostrador:Control) -> void:
	mostrador.queue_free()

func _on_mp_cambio(mp:int) -> void:
	pass

func _on_subio_nivel(nivel:int) -> void:
	pass

func _on_exp_incrementada() -> void:
	emit_signal("exp_incrementada")