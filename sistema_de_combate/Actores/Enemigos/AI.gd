extends Node
signal termino_pausa
signal ai_disponible
signal mandar_accion_data(ejecutor,objetivo,accion)


export(bool) var inactivo = false
onready var enemigos_cola  = get_node("../EnemigosPosition/cola")
onready var jugadores_cola = get_node("../JugadoresPosition/cola")
onready var timer = get_node("Timer")

################ Banderas ###########
var pausa:bool = false
var trabajando:bool = false
var actualizando:bool = false
#####################################

var random = RandomNumberGenerator.new()
var factor_actual
var enemigos = []

enum Factores { VIDA } 
enum Tipo_accion {AGRESIVA, PASIVA}

func _ready() -> void:
	connect("ai_disponible",self, "_on_ai_disponible")
	connect("termino_pausa", self, "_on_pausa_terminada")
	enemigos_cola.connect("listo", self, "_on_nodo_enemigos_listo")
	enemigos_cola.connect("cambio_cola",self,"actualizar_grupo_enemigos")
	jugadores_cola.connect("actor_empezo_accion",self,"_on_jugador_empezo_accion")
	timer.connect("timeout", self, "_on_timer_timeout")

func _on_ai_disponible() -> void:
	timer.start()

func _on_nodo_enemigos_listo() -> void:
	actualizar_grupo_enemigos()
	timer.wait_time = enemigos_cola.miembros[0].tiempo_espera
	timer.start()

func actualizar_grupo_enemigos() -> void:
	enemigos = enemigos_cola.miembros.duplicate()
	enemigos.shuffle()

func _on_jugador_empezo_accion(actor:Actor) -> void:
	pausa = true
	yield(actor,"comando_ejecutado")
	emit_signal("termino_pausa")

func _on_pausa_terminada() -> void:
	pausa = false

func _on_timer_timeout()  -> void:
	selecionar_enemigo()

func selecionar_enemigo() -> void:
	if inactivo:
		return

	revisar_grupo()
	if enemigos.empty(): return 

	var enemigo = enemigos.pop_back()
	revisar_grupo()

	if not is_instance_valid(enemigo):
		selecionar_enemigo()
	else:
		if hay_jugador_vivo():
			_Ai(enemigo)


func revisar_grupo() -> void:
	if enemigos.empty():
		actualizar_grupo_enemigos()

func hay_jugador_vivo() -> bool:
	var sobrevivientes = false
	for jugador in jugadores_cola.miembros:
		if jugador.vivo:
			sobrevivientes = true

	return sobrevivientes


################################ AI utils ################################################

func _Ai(en_turno:Actor) -> void:
	if _en_turno_valido(en_turno):
		factor_actual = _calcular_factor()
		var objetivo:Actor = _calcular_objetivo()
		var accion:String= _calcular_accion(en_turno)
		if objetivo != null and accion != "":
			if pausa:
				yield(self,"termino_pausa")
			if _en_turno_valido(en_turno):
				emit_signal("mandar_accion_data",en_turno,objetivo,accion)
		else:
			Debugger.log("Fallo en AI",name)
	emit_signal("ai_disponible")

func _en_turno_valido(en_turno:Actor) -> bool:
	return is_instance_valid(en_turno) and en_turno.vivo

func _calcular_factor() -> int:
	return Factores.VIDA

func _calcular_objetivo() -> Actor:
	if jugadores_cola.miembros.empty() or not hay_jugador_vivo():
		return null
	random.randomize()
	var probabilidad_factor = random.randf_range(0,1)
	if probabilidad_factor <= 0.2: #20% de probabilidad
		if factor_actual == Factores.VIDA:
			return _get_con_menos_vida()
		else:
			return _get_objetivo_random()
	else:
		return _get_objetivo_random()

func _get_objetivo_random() -> Actor:
	random.randomize()
	var index = random.randi_range(0,jugadores_cola.miembros.size() - 1)
	while(not jugadores_cola.miembros[index].vivo):
		index = random.randi_range(0,jugadores_cola.miembros.size() - 1)
	return jugadores_cola.miembros[index]

func _get_con_menos_vida() -> Actor:
	var menor_vida = GameDefinitions.Vida_limite
	var _con_menos_vida:Actor = null
	for jugador in jugadores_cola.miembros:
		if not jugador.vivo:
			continue
		if jugador.estadisticas.hp  < menor_vida:
			_con_menos_vida = jugador
			menor_vida = jugador.estadisticas.hp
	return _con_menos_vida
			

func _calcular_accion(enemigo_en_turno:Actor) -> String:
	if not is_instance_valid(enemigo_en_turno): return ""
	var accion_elegida = ""
	random.randomize()
	if factor_actual == Factores.VIDA:
		if random.randf_range(0,1) > 0.8: #20% de probabilidad de elegir la accion más ofensiva
			var relevancia_ofensiva = -1
			for accion in enemigo_en_turno.acciones_disponibles:
				if accion.tipo == Tipo_accion.AGRESIVA:
					if accion.relevancia > relevancia_ofensiva:
						accion_elegida = accion.accion
		else:
			accion_elegida = enemigo_en_turno.accion_atacar.accion

	return accion_elegida
