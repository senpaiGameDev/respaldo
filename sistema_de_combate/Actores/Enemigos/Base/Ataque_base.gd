class_name Ataque_base
extends Node2D

signal ataque_terminado

onready var audio_ataque = get_node("Audio")
onready var arma_slot = get_node("../ArmaSlot")

var arma = null

func _ready() -> void:
    if arma_slot.get_child_count() > 0:
        arma = arma_slot.get_child(0)
        arma.connect("ataque_finalizado",self,"_on_arma_ataque_finalizado")

func _on_arma_ataque_finalizado():
    arma_slot.visible = false
    emit_signal("ataque_terminado")

func atacar() -> void:
    if arma != null:
        arma_slot.visible = true
        arma.atacar()
    else:
        audio_ataque.play()
        emit_signal("ataque_terminado")

