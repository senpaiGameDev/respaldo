class_name Base
extends Actor

signal cambio_mp(mp)

export(float) var ataque_offset = 0
export(float) var monedas = 25

const DURACION_TAMABALEAR = 0.5
const DURACION_ACCIONAR = 1.0
const ANIMACION_CAMINAR = "Caminar"
const DISTANCIA_ACCIONAR:int = 50
const SUBIDA_X_NIVEL = 20

onready var tween = get_node("Tween")
onready var sprite = get_node("Sprite")
onready var animaciones = get_node("Animaciones")
onready var anim_ataque = get_node("Ataque")
onready var arma_nodo = get_node("ArmaSlot")
onready var magia:Magia = get_node("Magia")
onready var hurt_audio = get_node("HurtAudio")

var en_turno = false
var objetivo_actual: Actor
var posicion_original:Vector2 
var arma = null


enum Tipo_accion {AGRESIVA, PASIVA} #agresiva: atacar al objetivo, pasiva: curar aliados o asi mismo

class Accion:
	var tipo:int
	var accion:String
	var relevancia:int

	func _init(n_tipo, n_accion, n_relevancia) -> void:
		self.tipo = n_tipo
		self.accion = n_accion
		self.relevancia = n_relevancia


var accion_atacar = Accion.new(Tipo_accion.AGRESIVA,GameDefinitions.Accion_atacar,1)
var acciones_disponibles = [accion_atacar]
var magias_disponibles


####################### INICIALIZADORES ############################ 

func _ready() -> void:
	tween.connect("tween_completed",self,"_on_tween_completed")
	asignar_desde_estadisticas()
	_asignar_tiempo()
	anim_ataque.connect("ataque_terminado", self, "_on_ataque_ataque_terminado")
	if arma_nodo.get_child_count() > 0:
		arma = arma_nodo.get_child(0)
	posicion_original = position
	magia.load_gasto_x_magia_dicc()
	magia.load_animaciones_dicc()
	cargar_acciones_de_magias()

func cargar_acciones_de_magias() -> void:
	for accion_magia in magia.magias_disponibles_flags:
		var nueva_accion = Accion.new(Tipo_accion.AGRESIVA,accion_magia,2)
		acciones_disponibles.append(nueva_accion)
	magias_disponibles = magia.magias_disponibles_flags

func _asignar_tiempo() -> void:
	var random = RandomNumberGenerator.new()
	random.randomize()
	var tiempo_random = random.randf_range(0.5,1.5)
	tiempo_espera += tiempo_random

func asignar_desde_estadisticas() -> void:
	nivel = estadisticas.nivel
	vida_max = estadisticas.hp_max 
	mp_max = estadisticas.mp_max 
	fuerza = estadisticas.fuerza 
	poder_magico = estadisticas.poder_magico  
	tiempo_espera = estadisticas.tiempo_espera
	resistencia = estadisticas.resistencia 
	resistencia_magica = estadisticas.resistencia_magica 
	experiencia = estadisticas.experiencia 

####################################################################

######################### UTILIDADES ##################################

func ejecutar_accion(accion:String, objetivo:Actor) -> void:
	emit_signal("empezar_accion",self)
	puede_estar_en_turno  = false
	objetivo_actual = objetivo
	if accion == GameDefinitions.Accion_atacar:
		_atacar(objetivo)

	elif accion in magias_disponibles:
		_lanzar_hechizo(objetivo,accion)

	else:
		accion_terminada()
		Debugger.log("Accion aun no implementada", get_path())

	terminar_turno()


func calcular_fuerza_total() -> int:
	var n_fuerza = fuerza
	if arma != null:
		n_fuerza += arma.get_fuerza()
		if is_instance_valid(objetivo_actual):
			n_fuerza = (n_fuerza * 2) - (objetivo_actual.resistencia * 0.5)
		n_fuerza = clamp(n_fuerza,0,n_fuerza)
	return fuerza

func es_un_objetivo_valido() -> bool:
	return is_instance_valid(objetivo_actual) and objetivo_actual.vivo

func get_experiencia() -> int:
	return experiencia

func get_monedas() -> int:
	return monedas
######################## ACCIONES ############################

func _atacar(objetivo:Actor) -> void:
	if es_un_objetivo_valido():
		_animacion_atacar(objetivo_actual.global_position)
	else:
		accion_terminada()

func _lanzar_hechizo(objetivo:Actor, magia_nombre:String) -> void:
	if es_un_objetivo_valido():
		_animacion_inicio_accion()
		magia.ejecutar(objetivo,magia_nombre)
		yield(magia,"proceso_de_magia_terminado")
		finalizar_accion()
	else:
		accion_terminada()


###################### ANIMACIONES ##############################

func _animacion_atacar(objetivo_position:Vector2) -> void:
	#animación de caminar hacia enfrente
	var offset = ataque_offset
	var posicion_final_x = objetivo_position.x + offset
	var posicion_final = Vector2(posicion_final_x, objetivo_position.y)
	tween.interpolate_property(
		self,
		'global_position',
		global_position,
		posicion_final, 
		DURACION_ACCIONAR/2,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN
	)
	tween.start()
	animaciones.stop()

func _animacion_morir() -> void:
	tween.interpolate_property(
		self,
		"modulate",
		modulate,
		Color("00ffffff"), #transparente
		0.3,
		Tween.TRANS_LINEAR,
		Tween.EASE_OUT
	)
	tween.start()
	
func _animacion_recibir_agravio() -> void:
	if vivo:
		hurt_audio.play()
	tween.interpolate_method(
		self,
		"animacion_parpadera_de_rojo",
		0,
		6,
		DURACION_TAMABALEAR,
		Tween.TRANS_ELASTIC,
		Tween.EASE_IN_OUT
	)
	tween.start()

func animacion_parpadera_de_rojo(progreso:float) -> void:
	var color = Color(1,1,1,1) if int(round(progreso)) % 2 == 0 else Color.red
	if round(progreso) == 0:
		sprite.scale = Vector2(0.86,1.03)
	elif round(progreso) == 6:
		sprite.scale = Vector2(1,1)
	sprite.modulate =  color

func _animacion_inicio_accion() -> void:
	#direccion en x del objetivo:
	var direccion = Vector2.RIGHT
	#animación de caminar hacia enfrente
	tween.interpolate_property(
		self,
		'position',
		position,
		position + (DISTANCIA_ACCIONAR * direccion), 
		DURACION_ACCIONAR/2,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN
	)
	tween.start()
	animaciones.stop()

func finalizar_accion() -> void:
	sprite.flip_h = not sprite.flip_h
	animaciones.play(ANIMACION_CAMINAR)
	tween.interpolate_property(
		self,
		"position",
		position,
		posicion_original,
		DURACION_ACCIONAR/2,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN
	)
	tween.start()

##################### Callbacks de señales #########################
func terminar_turno() -> void:
	emit_signal("turno_finalizado",self)

func accion_terminada() -> void:
	emit_signal("comando_ejecutado")

func _on_ataque_ataque_terminado() -> void:
	if es_un_objetivo_valido():
		objetivo_actual.recibir_agravio(calcular_fuerza_total())
	finalizar_accion()

func _on_tween_completed(_objeto:Object,propiedad:NodePath) -> void:
	if propiedad == ":modulate":
		accion_terminada()
		terminar_turno()
		emit_signal("actor_muerto",self)
	if propiedad == ':position':
		if position == posicion_original:
			sprite.flip_h = not sprite.flip_h
			accion_terminada()
	elif propiedad == ":global_position":
		if anim_ataque:
			anim_ataque.atacar()
		
func _on_estadisticas_cambio_vida(vida:int,monto:int) -> void:
	if monto < 0:
		_animacion_recibir_agravio()
	if vida <= 0:
		_morir()

func _on_mp_cambio(mp:int) -> void:
	if mp <= 0:
		sin_magia = true
	emit_signal("cambio_mp",mp)

func _morir() -> void:
	vivo = false
	tween.stop_all()
	_animacion_morir()
