class_name Cola_posicion
extends Position2D

signal listo(cola)
var cola:Cola = null

#funcion ready para hacer pruebas
func _ready() -> void:
	cola = get_child(0)
	get_tree().create_timer(0.5).connect("timeout", self, "mandar_singal")

func mandar_singal():
	Debugger.log("cola_posicion:" + name + " listo",get_path())
	emit_signal("listo",cola)

func cargar(n_cola:Cola) -> void:
	add_child(n_cola)
	cola = n_cola
	emit_signal("listo", cola)

func get_miembros_cola(index_cola:int) -> Array:
	if index_cola <= get_child_count():
		return get_child(index_cola).miembros
	else:
		return []
