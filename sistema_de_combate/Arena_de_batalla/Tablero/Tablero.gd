class_name Tablero
extends Node
# La case tablero se encarga de crear un tablero donde cada actor de la batalla
# cuente con una posicion especifica. El tablero es un arreglo bidimensional 
# el cual contiene un arreglo de las colas de los actores en batalla.
# No solo crea el tablero también lo ordena para que sea coherente con lo que se muestra en interfaz
# esto quiere decir que la primera posición del tablero "tablero[0][0]" sea el actor que esta más 
# arriba a la izquierda. 
# Para lograr esto se ordena cada cola del eje_x del tablero de forma que el primer elemento de la
# cola sea el que tenga la posicion más arriba de la cola (position.y menor). Esto se ve en la clase
# "miOrdenamiento" la cual muestra el factor de ordenamiento.
# En cuanto el tablero ya este construido correctamente, (Con la posicion correcta de cada actor)
# se manda la señal de "listo" la cual indica que ya se puede ocupar el tablero.
# Para poder utilzar el tablero es necesario mandar a llamar a su función "inicializar()"
# Por el momento el sistema de combate esta diseñado para que solo existan máximo dos colas de 
# enemigos y una cola de jugadores donde el número maximo de cada cola es de 3 actores. 
# por lo tanto el tamaño máximo del tablero sería de 3X3.

signal listo
signal actualizacion_terminada
signal empezando_actualizacion
signal todos_los_enemigos_derrotados
signal todos_los_jugadores_derrotados

const NUMERO_DE_COLAS = 2
const ANIM_HACIA_DELANTE = "HaciaDelante"
const ANIM_HACIA_ATRAS = "HaciaAtras"

onready var enemigos_cola = get_node("EnemigosPosition/cola")
onready var jugadores_cola = get_node("JugadoresPosition/cola")
onready var animaciones = get_node("Animation")

var tablero:Array = []
var contador_colas = 0

class miOrdenamiento:
	static func ordenarEjeY(a:Position2D,b:Position2D) -> bool:
		if a.position.y < b.position.y:
			return true
		return false

func _ready() -> void:
	enemigos_cola.connect("listo", self, "_on_colas_posicion_listo")
	jugadores_cola.connect("cambio_cola", self, "actualizar")
	enemigos_cola.connect("cambio_cola",self,"actualizar")
	animaciones.connect("animation_finished",self,"_on_animation_finished")
	#jugadores_cola.connect("listo",self, "_on_colas_posicion_listo")

func _on_colas_posicion_listo() -> void:
	animaciones.play(ANIM_HACIA_DELANTE)
	_inicializar()

func actualizar() -> void: 
	if una_cola_vacia():
		return
	emit_signal("empezando_actualizacion")
	_construir_tablero()
	emit_signal("actualizacion_terminada")

func _on_animation_finished(anim_name:String) -> void:
	if anim_name == ANIM_HACIA_DELANTE:
		emit_signal("listo")

func _inicializar() -> void:
	_construir_tablero()

func _construir_tablero() -> void:
	if una_cola_vacia():
		return
	crear_tablero()
	ordenar_tablero()

func una_cola_vacia() -> bool:
	return checar_enemigos_muertos() || checar_jugadores_muertos()

func checar_enemigos_muertos() -> bool:
	if enemigos_cola.miembros.empty():
		animaciones.play(ANIM_HACIA_ATRAS)
		emit_signal("todos_los_enemigos_derrotados")
		return true
	return false

func checar_jugadores_muertos() -> bool:
	var todos = true
	for jugador in jugadores_cola.miembros:
		if jugador.vivo:
			todos = false
	if todos:
		emit_signal("todos_los_jugadores_derrotados")
		return true
	return false

func crear_tablero() -> void:
	tablero.clear()
	tablero.append(enemigos_cola.miembros)
	tablero.append(jugadores_cola.miembros)

func ordenar_tablero() -> void:
	for cola in tablero:
		cola.sort_custom(miOrdenamiento,"ordenarEjeY")
			

func imprimir_tablero():
	Debugger.log("Tablero: ", name)
	for fila in tablero:
		for actor in fila:
			Debugger.log("Actor: " + actor.name +" ",name)
