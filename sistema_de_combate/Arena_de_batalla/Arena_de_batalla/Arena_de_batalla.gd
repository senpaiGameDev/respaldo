class_name Arena_de_batalla
extends Node2D
# Clase Arena de batalla.
# Se encarga de  realizar las conexiones entre los diferentes nodos, 
# Por el momento se encarga de manejar el control entre las diferentes interfaces.
# Aqui se decide cual interface se muestra en que momento, incluyendo al selector
# cuando se muestra el selector y cuando se muestra la interfaz de comandos, asi como el manejo
# de las entradas entre cada uno de esto.

#TODO: re-diseñar esta clase/nodo ya que se tiene que instanciar desde la clase partida y asignase desde esa clase
#tanto a la cola de jugadores como a la cola de enemigos
signal arena_lista
signal termino_accion_enemigo
signal termino_batalla(victoria,arena_de_batalla,jugadores)
signal enemigos_listos(enemigos)
signal continuar(arena_batalla)

const TIEMPO_ENTRE_OLEADAS = 2
const TIEMPO_FIN_DE_BATALLA = 1.5
const pausa_menu_ref = preload("res://Interfaces/PausaBatalla/PausaBatalla.tscn")
const siguiente_oleada_anim_ref = preload("res://sistema_de_combate/Arena_de_batalla/SiguienteOleadaAnimacion.tscn")

onready var tablero = get_node("Tablero")
onready var enemigos:Cola = get_node("Tablero/EnemigosPosition/cola")
onready var jugadores:Cola = get_node("Tablero/JugadoresPosition/cola")
onready var selector = get_node("Interfaz/Selector")
onready var comandos = get_node("Interfaz/Interfaz_batalla/ContenedorInterfaces/ContenedorComandos/Interfaz_comandos")
onready var interfaz_grupal = get_node("Interfaz/Interfaz_batalla/ContenedorInterfaces/ContenedorGrupo/Interfaz_grupal")
onready var audio = get_node("Audio")
onready var interfaz_batalla = get_node("Interfaz/Interfaz_batalla")
onready var fondo_nodo:TextureRect = get_node("Fondo")
onready var controlador_acciones = get_node("Controlador_acciones")
onready var interfaz_nodo = get_node("Interfaz")
onready var siguiente_oleada_anim_posicion = get_node("Interfaz/SiguienteOleadaAnimPosicion")
onready var oleadas_gui = get_node("Interfaz/OleadasGUI")
onready var camara = get_node("Camera")
onready var pantalla_victoria = get_node("Interfaz/VictorioPantalla")

var tablero_preparado = false
var actor_en_turno: Actor = null
var objetivo_seleccinado:Actor = null
var accion = ''
var en_pausa = false
var actualizando_tablero = false
var fin_de_batalla = false

var audiostream:AudioStream
var fondo:Texture
var oleadas:int
var enemigos_disponibles = []
var respawn_timer:Timer

var min_num_enemigos_x_oleada = 2
var max_num_enemigos_x_oleada = 3

func _ready() -> void:
	_set_config()
	camara.make_current()
	tablero.connect("listo",self,"_on_tablero_inicializado")
	tablero.connect("empezando_actualizacion",self,"_on_tablero_empieza_actualizacion")
	tablero.connect("actualizacion_terminada", self, "_on_tablero_actualizacion_terminada")
	tablero.connect("todos_los_enemigos_derrotados", self, "_on_todos_los_enemigos_derrotados")
	tablero.connect("todos_los_jugadores_derrotados", self, "_on_todos_los_jugadoes_derrotados")
	enemigos.connect("actor_empezo_accion",self,"_on_enemigo_empezo_accion")
	enemigos.connect("listo",self,"_on_enemigos_listo")
	jugadores.connect("cambio_turno",self,"_on_cola_cambio_turno")
	selector.connect("mandar_objetivo",self,"_on_selector_recibir_objetivo")
	selector.connect("cancelar",self,"_on_selector_cancelar")
	comandos.connect("mandar_accion",self,"_on_comandos_recibir_accion")
	pantalla_victoria.connect("continuar",self,"_on_continuar")
	connect("termino_accion_enemigo", self, "_on_termino_accion_enemigo")
	cargar_jugadores()
	spawnear_oleada()

func _process(delta: float) -> void:
	if Input.is_action_just_pressed("ui_select"):
		_pausa()

func _pausa() -> void:
	var menu = pausa_menu_ref.instance()
	interfaz_nodo.add_child(menu)
	get_tree().paused = true


func _set_config() -> void:
	oleadas_gui.inicializar(oleadas)
	fondo_nodo.set_texture(fondo)
	audio.set_stream(audiostream)
	audio.play()
	

func cargar_jugadores() -> void:
	for jugador in jugadores.miembros:
		interfaz_grupal.agregar_nuevo_integrante(jugador)


func _on_tablero_inicializado() -> void:
	if siguiente_oleada_anim_posicion.get_children().size() > 0:
		siguiente_oleada_anim_posicion.get_child(0).queue_free()
		config_nodo(interfaz_batalla,true)
		if actor_en_turno != null and is_instance_valid(actor_en_turno):
			comandos.entrar(actor_en_turno)
	else:
		jugadores.atender_primero()

	selector.inicializar(tablero)
	emit_signal("arena_lista")

func _on_enemigos_listo() -> void: 
	emit_signal("enemigos_listos",enemigos.miembros.duplicate())

func _on_selector_recibir_objetivo(objetivo:Actor) -> void:
	config_nodo(selector,false) #si ya mandamos al objetivo no necesitamos más al selector
	if objetivo == null:
		return
	objetivo_seleccinado = objetivo
	if actor_en_turno != null and accion != '':
		if en_pausa:
			yield(self,"termino_accion_enemigo")
		controlador_acciones.agregar_accion(actor_en_turno,objetivo,accion)
		jugadores.atender_primero()

# Cuando se recibe el comando por parte del usuario y este no es el comando "pasar" 
# entonces cambiamos el control de la "interfaz_de_comandos" 
# al "selector", esto lo hacemos desactivando las entradas de "la interfaz_de_comandos" 
# y activando las entradas del "selector"
func _on_comandos_recibir_accion(accion_recv:String) -> void:
	self.accion = accion_recv
	if accion_recv == GameDefinitions.Accion_pasar:
		actor_en_turno.ejecutar_accion(accion,null)
		jugadores.atender_primero()
		config_nodo(comandos,true)
	else:
		config_nodo(comandos,false)
		config_nodo(selector,true)
		if actualizando_tablero:
			yield(tablero,"actualizacion_terminada")
		selector.entrar(actor_en_turno)

#Esta fución se ejecuta cuando el actor termina su turno.
#Cuando se termina el turno del actor se revisa que los datos(icono y nombre) esten asignados
#correctamente ya que son datos que se ocupan en la interfaz de comandos.
#Si No existe el error entonces activamos las entradas para la "interfaz_de_comandos" 
func _on_cola_cambio_turno(actor:Actor) -> void:
	if not fin_de_batalla:
		cargar_interfaz_comandos(actor)

#Se encarga de cargar la interfaz de comandos correspondiente al actor en turno.
#dicho actor en turno debe tener tanto nombre como icono previemente asignados.
#ya que son datos ocupados por la interfaz
func cargar_interfaz_comandos(actor:Actor) -> void:
	config_nodo(comandos,true)
	comandos.entrar(actor)
	actor_en_turno = actor
	accion = ''
	objetivo_seleccinado = null


# Para evitar que las dos interfaces que manejan entradas por el momento
#(Selector y Interfaz_comandos), entren en conflicto entre si.
# Se para el proceso de recibir entradas de una cuando la otra tiene el control y es la 
# interfaz activa. 
func config_nodo(nodo:Object, valor:bool) -> void:
	nodo.set_process_input(valor)
	nodo.set_process_unhandled_input(valor)
	nodo.set_process_unhandled_key_input(valor)
	nodo.set_process(valor)
	nodo.visible = valor

func _on_tablero_actualizacion_terminada() -> void:
	if not fin_de_batalla:
		selector.inicializar(tablero)
		actualizando_tablero = false

func _on_tablero_empieza_actualizacion() -> void:
	actualizando_tablero = true

#si cancelamos el comando seleccionado entonces quitamos al selector y cargamos
#la interfaz de comandos con el mismo actor en turno
func _on_selector_cancelar() -> void:
	config_nodo(selector,false)
	if actor_en_turno.vivo:
		cargar_interfaz_comandos(actor_en_turno)

func _on_enemigo_empezo_accion(actor:Actor) -> void:
	en_pausa = true
	var selector_activo = selector.visible
	if selector_activo:
		config_nodo(selector,false)
	config_nodo(interfaz_batalla,false)
	yield(actor,"comando_ejecutado")
	if fin_de_batalla:
		return
	config_nodo(interfaz_batalla,true)
	if actor_en_turno.vivo:
		_recargar_con_mismo_actor(selector_activo)
	else:
		jugadores.atender_primero()
	emit_signal("termino_accion_enemigo")

func _recargar_con_mismo_actor(selector_activo:bool) -> void:
	if selector_activo:
		config_nodo(comandos,false)
		config_nodo(selector,true)
	else:
		comandos.entrar(actor_en_turno)

func _on_termino_accion_enemigo() -> void:
	en_pausa = false

func _on_todos_los_enemigos_derrotados() -> void:
	oleadas -= 1
	if oleadas > 0:
		oleadas_gui.actualizar(oleadas)
		config_nodo(interfaz_batalla,false)
		_animacion_entre_oleadas()
		yield(get_tree().create_timer(TIEMPO_ENTRE_OLEADAS),"timeout")
		spawnear_oleada()
	else:
		_on_fin_de_batalla()
		victoria()

func _animacion_entre_oleadas() -> void:
	var oleada_anim = siguiente_oleada_anim_ref.instance()
	siguiente_oleada_anim_posicion.add_child(oleada_anim)

func spawnear_oleada() -> void:
	if max_num_enemigos_x_oleada == 1:
		enemigos.llenar_cola(enemigos_disponibles,1)
	else:
		var random = RandomNumberGenerator.new()
		random.randomize()
		var probabilidad = random.randf_range(0,1)
		var enemigos_en_oleada = min_num_enemigos_x_oleada
		if probabilidad > 0.7:
			enemigos_en_oleada = min_num_enemigos_x_oleada
		elif probabilidad > 0.5:
			enemigos_en_oleada = max_num_enemigos_x_oleada - 1
		else:
			enemigos_en_oleada = max_num_enemigos_x_oleada

		enemigos.llenar_cola(enemigos_disponibles,enemigos_en_oleada)

func victoria() -> void:
	Debugger.log("VICTORIA",name)
	yield(get_tree().create_timer(TIEMPO_FIN_DE_BATALLA),"timeout")
	emit_signal("termino_batalla",true,self,jugadores.miembros)

func _on_todos_los_jugadoes_derrotados () -> void:
	_on_fin_de_batalla()
	yield(get_tree().create_timer(TIEMPO_FIN_DE_BATALLA),"timeout")
	emit_signal("termino_batalla",false,self,jugadores.miembros)

func _on_fin_de_batalla() -> void:
	oleadas_gui.visible = false
	controlador_acciones.eliminar_acciones()
	fin_de_batalla = true
	audio.stop()
	config_nodo(interfaz_batalla,false)

func config_oleadas(min_num_enemigos:int, max_num_enemigos:int) -> void:
	min_num_enemigos_x_oleada = min_num_enemigos
	max_num_enemigos_x_oleada = max_num_enemigos

func config_arena(n_fondo:Texture,musica:AudioStream, numero_de_oleadas:int,enemigos_n:Array) -> void:
	fondo = n_fondo
	audiostream = musica
	oleadas = numero_de_oleadas
	enemigos_disponibles = enemigos_n.duplicate()
	
func mostrar_ganancias_batalla(monedas:int,jugadores_subieron_de_nivel:Array,exp_n:int) -> void:
	pantalla_victoria.set_info(exp_n,jugadores_subieron_de_nivel,monedas)
	pantalla_victoria.show()

func _on_continuar() -> void:
	emit_signal("continuar",self)
