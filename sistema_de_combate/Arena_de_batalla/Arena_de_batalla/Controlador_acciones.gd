extends Node

onready var cola_jugadores = get_node("../Tablero/JugadoresPosition/cola")
onready var cola_enemigos = get_node("../Tablero/EnemigosPosition/cola")
onready var ai = get_node("../Tablero/AI")

class AccionData:
	var ejecutor: Actor
	var ejecutor_nombre : String
	var objetivo: Actor
	var objetivo_nombre:String
	var accion: String
	var ejecutor_tipo:int
	var objetivo_tipo: int

	func _init(ejecutor_n,objetivo_n,accion_n,ejecutor_tipo_n, objetivo_tipo_n):
		self.ejecutor = ejecutor_n
		self.ejecutor_nombre = ejecutor_n.name
		self.objetivo = objetivo_n
		self.objetivo_nombre = objetivo_n.name
		self.accion = accion_n
		self.ejecutor_tipo =  ejecutor_tipo_n
		self.objetivo_tipo = objetivo_tipo_n

	func imprimir_accion(posicion:int) -> void:
		var mensaje = "Accion { Ejecutor: %s, accion: %s, Objetivo: %s, posicion: %d }" \
		 % [self.ejecutor_nombre, self.accion, self.objetivo_nombre, posicion]
		print("[Controlador_acciones]::%s" % mensaje)

var trabajando = false
var acciones = []

func _ready() -> void:
	ai.connect("mandar_accion_data",self,"agregar_accion")

func _process(_delta: float) -> void:
	if not trabajando and acciones.size() > 0:
		_trabajar_con_siguiente_accion()

func _trabajar_con_siguiente_accion() -> void:
	trabajando = true
	if acciones.empty(): 
		trabajando = false
		return 

	var accion_data_actual = acciones.pop_front()

	if is_instance_valid(accion_data_actual.ejecutor) && accion_data_actual.ejecutor.vivo:
		if is_instance_valid(accion_data_actual.objetivo):
			_ejecutar_accion_de_data(accion_data_actual)
		else:
			_buscar_nuevo_objetivo(accion_data_actual)
		return;

	_terminar_accion(accion_data_actual)

func _terminar_accion(_accion_data) -> void:
	trabajando = false

func _buscar_nuevo_objetivo(accion_data:AccionData) -> void:
	if accion_data.objetivo_tipo == GameDefinitions.TipoDeActor.ENEMIGO: 
		var nuevo_objetivo = _nuevo_objetivo(cola_enemigos)
		if nuevo_objetivo != null:
			accion_data.objetivo = nuevo_objetivo
			_ejecutar_accion_de_data(accion_data)
		else:
			_ejecutar_accion_pasar(accion_data)
			return
	else:
		var nuevo_objetivo = _nuevo_objetivo(cola_jugadores)
		if nuevo_objetivo != null:
			accion_data.objetivo = nuevo_objetivo
			_ejecutar_accion_de_data(accion_data)
		else:
			_ejecutar_accion_pasar(accion_data)
			return


func _nuevo_objetivo(cola) -> Actor:
	var actores = cola.get_children()
	if actores.empty():
		return null
	return actores[0]

func _ejecutar_accion_pasar(accion_data:AccionData):
	var objetivo = accion_data.objetivo
	var ejecutor = accion_data.ejecutor
	var accion = GameDefinitions.Accion_pasar
	ejecutor.ejecutar_accion(accion,objetivo)
	_terminar_accion(accion_data)

func _ejecutar_accion_de_data(accion_data:AccionData):
	var objetivo = accion_data.objetivo
	var ejecutor = accion_data.ejecutor
	var accion = accion_data.accion
	ejecutor.ejecutar_accion(accion,objetivo)
	yield(ejecutor,"comando_ejecutado")
	yield(get_tree().create_timer(0.25),"timeout")
	_terminar_accion(accion_data)

func agregar_accion(ejecutor:Actor, objetivo:Actor, accion:String) -> void:
	var objetivo_tipo = GameDefinitions.TipoDeActor.ENEMIGO if objetivo is Base else GameDefinitions.TipoDeActor.JUGADOR
	var ejecutor_tipo = GameDefinitions.TipoDeActor.JUGADOR if ejecutor is Jugador else GameDefinitions.TipoDeActor.ENEMIGO
	var nueva_accion = AccionData.new(ejecutor,objetivo,accion,ejecutor_tipo,objetivo_tipo)
	acciones.append(nueva_accion)

func imprimir_cola() -> void:
	for i in range(len(acciones)):
		acciones[i].imprimir_accion(i)

func eliminar_acciones() -> void:
	acciones.clear()
