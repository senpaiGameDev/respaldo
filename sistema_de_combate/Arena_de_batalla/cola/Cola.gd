class_name Cola
extends Node2D

const MAX_SIZE = 3
const MIN_SIZE = 1

signal cambio_turno(actor)
signal cambio_cola
signal _actor_formado_en_cola
signal actor_empezo_accion(actor)
signal listo
enum TipoDeCola {ENEMIGOS,JUGADORES}

export(TipoDeCola) var tipo

var grupo = []
var cola  = []
var miembros = []
var en_turno :Actor = null

var localizacion_grupo:Dictionary = {
	TipoDeCola.ENEMIGOS:[
		Vector2(-10,-26),
		Vector2(38,46),
		Vector2(-18,118)
	],
	TipoDeCola.JUGADORES:[
		Vector2(36,-26),
		Vector2(-20,46),
		Vector2(36,118)
	]
}

func _ready() -> void:
	if tipo == TipoDeCola.JUGADORES:
		_inicializar()


func llenar_cola(enemigos_disponibles:Array,num_enemigos:int) -> void:
	var random = RandomNumberGenerator.new()
	if num_enemigos >= MIN_SIZE and num_enemigos <= MAX_SIZE:

		for _i in range(num_enemigos):
			random.randomize()
			var barajear = random.randi_range(5,10)
			for _baraja in range(barajear):
				enemigos_disponibles.shuffle()
			var index = random.randi_range(0,enemigos_disponibles.size() - 1)
			grupo.append(enemigos_disponibles[index])

		_inicializar()

func _inicializar() -> void:
	_agregar_grupo()
	miembros = get_children()
	inicializar_actores()
	emit_signal("listo")

func _agregar_grupo() -> void:
	var localizaciones = localizacion_grupo.get(tipo)
	var referencias = []
	if tipo == TipoDeCola.ENEMIGOS:
		referencias = grupo.duplicate(true)
		grupo.clear()
	else:
		referencias = GameManager.get_grupo_activo()	
	for i in range(referencias.size()):
		var load_ref = load(referencias[i])
		var instancia = load_ref.instance()
		instancia.position = localizaciones[i]
		add_child(instancia)


func inicializar_actores() -> void:
	for actor in get_children():
		actor.connect("empezar_accion",self, "_on_actor_empezo_accion")
		actor.connect("turno_finalizado",self,"_on_actor_turno_finalizado")
		actor.connect("actor_muerto",self,"_on_actor_muerto")
		actor.accion_terminada()
		actor.terminar_turno()
		if actor is Jugador:
			actor.connect("jugador_muerto", self, "actualizar_cola_jugadores")
			actor.connect("jugador_revido", self, "actualizar_cola_jugadores")

#si el primero de la cola sigue ejecutando un comando
#entonces congelamos el turno hasta que se mande la señal de
# 'comando_ejecutado' para 'pausar' la cola y no sobreescribir comandos
#La primera verificación se realiza por si aun no existen actores formados en la cola.
#por que su tiempo de espera aun no ha terminado. si aun no hay un actor formado
#la función se congela hasta que se mande la señal "actor_formado_en_cola"
func atender_primero() -> void:
	if cola.empty():
		yield(self,'_actor_formado_en_cola')

	if not cola.empty():
		en_turno = cola.pop_front()
	else:
		return

	if not is_instance_valid(en_turno):
		return 

	#if not en_turno.puede_estar_en_turno:
	#	yield(en_turno,"comando_ejecutado")

	if not en_turno.vivo:
		_formar_en_cola(en_turno)
		atender_primero()

	emit_signal("cambio_turno",en_turno)

func _on_actor_turno_finalizado(actor:Actor) -> void:
	#if en_turno == actor || en_turno == null:
	#	atender_primero()
	_formar_en_cola(actor)

func _formar_en_cola(actor:Actor) -> void:
	#se congela hasta que el tiempo de espera del actor termine
	yield(actor,"tiempo_espera_terminado")
	cola.push_back(actor)
	emit_signal("_actor_formado_en_cola")

#Si el actor se elimina a si mismo, la cola no sabe que ya no tiene a ese hijo.
#Esto Trae muchos problemas ya que el tablero al momento de actualizarse seguira
#poniendo todas las posiciones como elegibles y el usuario podrá seleccionar a un
#actor que ya no existe.
#por eso es que debe de ser la cola la que se encargue de eliminar al actor.
#El tablero es el encargado de eliminar a la cola cuando esta se encuentre vacía
#Esto con la intención de evitar errores al actualizar el tablero
func _on_actor_muerto(hijo:Actor) -> void:
	remove_child(hijo)
	hijo.queue_free()
	miembros = get_children()
	var index = cola.find(hijo)
	if index >= 0:
		cola.remove(index)
	if cola.size() > 0:
		atender_primero()
	else:
		cola = get_children()
	emit_signal("cambio_cola") 

func _on_actor_empezo_accion(actor:Actor) -> void:
	emit_signal("actor_empezo_accion",actor)

func  actualizar_cola_jugadores() -> void:
	emit_signal("cambio_cola") 
