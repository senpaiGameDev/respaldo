class_name Animacion_magia
extends Node2D

signal animacion_terminada

export(float) var DURACION_ANIMACION = 0.4

onready var efecto_de_sonido_magia = get_node("EfectoDeSonidoMagia")
onready var animaciones = get_node("Animaciones")
onready var particulas = get_node("Particulas")

var animacion_completa = []
var animacion_actual = 0

func _ready() -> void:
	animacion_completa = animaciones.get_children()
	particulas.emitting = false
	for animacion in animacion_completa:
		animacion.position = position
		animacion.visible = false
		animacion.connect("animation_finished",self,"siguiente_animacion")
	
	#get_tree().create_timer(1.0).connect('timeout',self,"correr_animacion")

func correr_animacion() -> void:
	animacion_completa[animacion_actual].visible = true
	animacion_completa[animacion_actual].play()

func siguiente_animacion() -> void:
	animacion_actual += 1
	animacion_completa[animacion_actual - 1].visible = false
	animacion_completa[animacion_actual - 1].stop()
	if animacion_actual > animacion_completa.size() - 1: 
		efecto_de_sonido_magia.play()
		get_tree().create_timer(DURACION_ANIMACION).connect('timeout',self,"parar_particulas")
		particulas.visible = true
		particulas.emitting = true
	else:
		correr_animacion()

func parar_particulas() -> void:
	particulas.emitting = false
	emit_signal("animacion_terminada")
