class_name Magia
extends Node

signal proceso_de_magia_terminado


export(Array, String, "", #Error de godot, nunca cuenta al primer elemento.
	"Fulgur", #1 MagicDefinitions.fulgur_key
	"Aqua", #2 MagicDefinitions.aqua_key
	"Sana", #3 MagicDefinitions.sana_key
	"Ignis" #4 MagicDefinitions.ignis_key
	) var magias_disponibles_flags


var ultima_animacion:Animacion_magia = null
const ERROR_MSG_NO_VALUE = "Error en Magia.gd: Debes de Asignar todos los valores de la variable" +  \
					 "'magias_disponibles_flags' en todos los jugadores"

const ERROR_JUGADOR = "Error en Magia.gd: La magia no esta asignada a una juador concreto," + \
					" ya que tiene referencia nula"

var jugador:Actor = null

var animacion_x_magia:Dictionary = { }
var gasto_x_magia:Dictionary = { }
var magias_disponibles_actualmente = []

###################################### INICIALIZADORES ################################
func _ready() -> void:
	assert(_todas_las_magias_asignadas(), ERROR_MSG_NO_VALUE)
	MagicDefinitions.assert_magias_validas(magias_disponibles_flags)
	jugador = get_owner()
	assert(jugador != null,ERROR_JUGADOR)
	jugador.connect("cambio_mp", self, "_on_personaje_cambio_mp")

	################## Inicializadores externos ##########################################
func load_gasto_x_magia_dicc() -> void:
	for magia_disponible in magias_disponibles_flags:
		var gasto_normal = MagicDefinitions.get_gasto(magia_disponible)
		gasto_x_magia[magia_disponible] = gasto_normal + (gasto_normal * jugador.estadisticas.nivel) * 0.25
		if gasto_x_magia[magia_disponible] < jugador.mp_max:
			magias_disponibles_actualmente.append(magia_disponible)

func load_animaciones_dicc() -> void:
	for magia in magias_disponibles_flags:
		if magia == MagicDefinitions.fulgur_key:
			animacion_x_magia[MagicDefinitions.fulgur_key] = preload("res://sistema_de_combate/Magia/magias_animaciones/Fulgur_Anim.tscn")
		elif  magia == MagicDefinitions.aqua_key:
			animacion_x_magia[MagicDefinitions.aqua_key] = preload("res://sistema_de_combate/Magia/magias_animaciones/Aqua_Anim.tscn")
		elif  magia  == MagicDefinitions.sana_key:
			animacion_x_magia[MagicDefinitions.sana_key] = preload("res://sistema_de_combate/Magia/magias_animaciones/Sana_Anim.tscn")
		elif magia == MagicDefinitions.ignis_key:
			animacion_x_magia[MagicDefinitions.ignis_key] = preload("res://sistema_de_combate/Magia/magias_animaciones/Ignis_Anim.tscn")
	#########################################################################################

func _todas_las_magias_asignadas() -> bool:
	for magia in magias_disponibles_flags:
		if magia == "":
			return false
	return true

####################################################################################################

########################################### UTILIDADES ##############################################

func ejecutar(objetivo:Actor, hechizo_key:String) -> void:
	MagicDefinitions.assert_magia_valida(hechizo_key)
	var animacion = _instanciar_animacion(hechizo_key,objetivo.global_position)

	ultima_animacion = animacion
	var fuerza_total
	if hechizo_key != MagicDefinitions.sana_key:
		fuerza_total = _calc_fuerza_total(MagicDefinitions.Magias_dicc[hechizo_key].fuerza,objetivo)
	else:
		fuerza_total = _calc_fuerza_curativa(MagicDefinitions.Magias_dicc[hechizo_key].fuerza)
	_proceso_de_magia(animacion,objetivo,fuerza_total)
	jugador.cambiar_poder_magico(MagicDefinitions.Magias_dicc[hechizo_key].gasto)


func _calc_fuerza_curativa(fuerza_cruda:int) -> int:
	var fuerza_total = fuerza_cruda - jugador.poder_magico
	if jugador.arma != null:
		fuerza_total -= jugador.arma.get_magia()
	return (fuerza_total)

func _calc_fuerza_total(fuerza_cruda_magia:int,objetivo:Actor) -> int:
	fuerza_cruda_magia += (fuerza_cruda_magia * (jugador.nivel / 10))
	var fuerza_total = fuerza_cruda_magia + jugador.poder_magico 
	if is_instance_valid(objetivo):
		if jugador.arma != null:
			fuerza_total += jugador.arma.get_magia()
		fuerza_total = (fuerza_total * 1.25) - (objetivo.resistencia_magica * 0.5)
	fuerza_total = clamp(fuerza_total,0,fuerza_total)
	return fuerza_total


func _instanciar_animacion(magia_key:String,posicion:Vector2) -> Animacion_magia:
	var animacion = animacion_x_magia[magia_key].instance()
	animacion.global_position = posicion
	add_child(animacion)
	return animacion

func _proceso_de_magia(animacion:Animacion_magia,objetivo:Actor,magia_fuerza:int):
	animacion.correr_animacion()
	yield(animacion,"animacion_terminada")

	var timer = get_tree().create_timer(animacion.DURACION_ANIMACION)
	timer.connect("timeout",self,"_terminar",[objetivo,magia_fuerza])

func _terminar(objetivo:Actor,magia_fuerza:int) -> void:
	if is_instance_valid(ultima_animacion):
		ultima_animacion.queue_free()
	if is_instance_valid(objetivo):
		objetivo.recibir_agravio(magia_fuerza)
	emit_signal("proceso_de_magia_terminado")

func magia_disponible(magia:String) -> bool:
	return  magia in magias_disponibles_actualmente

########################################################################################################

##################################### SIGNAL CALLBACKS ################################################

func _on_personaje_cambio_mp(mp:int) -> void:
	for magia in magias_disponibles_flags:
		if gasto_x_magia.get(magia) > mp:
			if magia in magias_disponibles_actualmente:
				magias_disponibles_actualmente.erase(magia)
		else:
			if not magia in magias_disponibles_actualmente:
				magias_disponibles_actualmente.append(magia)

#######################################################################################################
