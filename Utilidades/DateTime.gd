class_name DateTime
extends Object

var rawTime
var second
var minute
var hour


func _init(time:float) -> void:
	rawTime = time
	_formatear()

func _formatear() -> void:
	var init_time = int(floor(rawTime))
	second = init_time % 60
	minute = (init_time / 60 ) % 60
	hour = (init_time / (60 * 60))

func add_time(time:float) -> void:
	rawTime += time

func get_text() -> String:
	_formatear()
	return "%02d:%02d:%02d" % [hour,minute,second]
