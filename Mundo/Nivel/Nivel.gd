class_name Nivel
extends Node2D

signal entro_nave(popup)
signal salio_nave(popup)
signal batalla_iniciada
signal batalla_terminada

var popup_ref = preload("res://Mundo/EntrarANivelPopup/EntrarANivelPopup.tscn")

onready var entrada_area = get_node("AreaEntrada")
#onready var popup_posicion:Position2D = get_node("popPosition")
#onready var popup:PopupPanel = get_node("popPosition/Nivel_UI")
onready var sub_niveles_contenedor = get_node("Sub_niveles")

var sub_niveles = []
var on_popup = false
var popup


func _ready() -> void:
	entrada_area.connect("area_entered",self,"_on_nave_entered")
	entrada_area.connect("area_exited",self,"_on_nave_exited")
	sub_niveles = sub_niveles_contenedor.get_children().duplicate(true)
	for sub_nivel in sub_niveles:
		sub_nivel.connect("batalla_terminada",self,"_on_batalla_terminada")
		sub_nivel.connect("batalla_iniciada", self, "_on_batalla_iniciada")

	_inicializar_gui()


func _inicializar_gui() -> void:
	popup = popup_ref.instance()
	popup.iniciar(sub_niveles)

func _on_sub_nivel_batalla_iniciada() -> void:
	emit_signal("salio_nave",popup)

func _on_nave_entered(_area: Area2D) -> void:
	emit_signal("entro_nave",popup)

func _on_nave_exited(_area:Area2D) -> void:
	emit_signal("salio_nave",popup)

func _on_batalla_terminada(sub_nivel_id:int) -> void:
	GameManager.partida.sub_nivel_completado(sub_nivel_id)
	popup.set_sub_nivel_ui_completado_por_id(sub_nivel_id)
	emit_signal("batalla_terminada")

func _on_batalla_iniciada() -> void:
	emit_signal("batalla_iniciada")
	
func entrar_en_sub_nivel(sub_nivel_index:int) -> void:
	if sub_nivel_index < sub_niveles.size():
		sub_niveles[sub_nivel_index].lanzar_batalla()
