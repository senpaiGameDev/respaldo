extends Node2D

signal batalla_iniciada
signal batalla_terminada(sub_nivel_id)
signal mostrar_ganancias

const despuest_de_batalla_scn = ""

export(int) var id
export(String) var sub_nivel_nombre = ""
export(Texture) var fondo = null
export(AudioStream) var musica = null
export(int,1,15) var oleadas = 0
export(int) var min_numero_de_enemigos_x_oleada = 1
export(int) var max_numero_de_enemigos_x_oleada = 3
export(String,
	"",
	"Capital", #GameDefinitions.capital_key
	"Praderas", #GameDefinitions.praderas_key
	"Castillo Demoniaco", # GameDefinitions.castillo_demoniaco
	"Torre Del Castillo Demoniaco" #GameDefinitions.torre_final
	) var nivel_raiz = ""

var enemigos_de_nivel = []
var enemigos_en_batalla

var contador_exp_x_batalla = 0
var contador_monedas_x_batalla = 0
var cargas_estadisticas_terminadas = 0
var jugadores_subieron_de_nivel = []

func lanzar_batalla() -> void:
	enemigos_de_nivel = GameDefinitions.niveles[nivel_raiz].get("enemigos")
	var arena_de_batalla = GameManager.arena_de_batalla_scn.instance()
	arena_de_batalla.config_arena(fondo,musica,oleadas,enemigos_de_nivel)
	arena_de_batalla.config_oleadas(min_numero_de_enemigos_x_oleada,max_numero_de_enemigos_x_oleada)
	arena_de_batalla.connect("termino_batalla",self,"_on_arena_termino_batalla")
	arena_de_batalla.connect("enemigos_listos",self,"_on_enemigos_listos")
	arena_de_batalla.connect("continuar",self,"_on_continuar")
	emit_signal("batalla_iniciada")
	SceneManager.agregar_escena(arena_de_batalla)


func _on_arena_termino_batalla(victoria:bool,arena_de_batalla:Arena_de_batalla, jugadores:Array) ->void:
	if not victoria:
		SceneManager.cambiar_escena(GameManager.game_over_scn)
		yield(SceneManager,"scene_changed")
	else:
		_repartir_ganancias(jugadores)
		#yield(self,"mostrar_ganancias")
		arena_de_batalla.mostrar_ganancias_batalla(contador_monedas_x_batalla,jugadores_subieron_de_nivel,contador_exp_x_batalla)
		#_salir_de_batalla(arena_de_batalla)

func _repartir_ganancias(jugadores:Array) -> void:
	for jugador in jugadores:
		if jugador.vivo:
			jugador.connect("subio_nivel",self, "_on_jugador_subio_nivel")
			jugador.connect("exp_incrementada",self,"_on_jugador_exp_incrementada")
			jugador.agregar_exp(contador_exp_x_batalla)
	GameManager.partida.inventario.agregar_monedas(contador_monedas_x_batalla)

func _mostrar_ganancias() -> void:
	emit_signal("mostrar_ganancias")

func resetear() -> void:
	contador_monedas_x_batalla = 0
	contador_exp_x_batalla = 0
	jugadores_subieron_de_nivel.clear()

func _on_jugador_subio_nivel(jugador_nombre:String,nivel:int) -> void:
	jugadores_subieron_de_nivel.append({"nombre":jugador_nombre,"nivel":nivel})

func _on_jugador_exp_incrementada() -> void:
	cargas_estadisticas_terminadas += 1
	if cargas_estadisticas_terminadas >= GameManager.get_grupo_activo().size():
		ganancias_repartidas()

func ganancias_repartidas() -> void:
		_mostrar_ganancias()

func _salir_de_batalla(arena_de_batalla) -> void:
	resetear()
	SceneManager.fade_in_out_animation(1)
	yield(SceneManager,"fade_in_completed")
	arena_de_batalla.queue_free()
	emit_signal("batalla_terminada",id)

func _on_enemigos_listos(enemigos:Array) -> void:
	for enemigo in enemigos:
		contador_exp_x_batalla += enemigo.get_experiencia()
		contador_monedas_x_batalla += enemigo.get_monedas()

func _on_continuar(arena_de_batalla:Arena_de_batalla) -> void:
	_salir_de_batalla(arena_de_batalla)
