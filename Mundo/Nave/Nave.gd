class_name Nave
extends KinematicBody2D

signal cambio_posicion(posicion)
export(float) var rapidez: = 0.0
export(float) var rapidez_max: = 0.0

const MoverSobreAireStrategy = preload("res://Mundo/Nave/MoverSobreAireStrategy.gd")
onready var animation_tree = get_node("AnimationTree")
onready var animation_state = animation_tree.get("parameters/playback")
onready var remote_node:RemoteTransform2D = get_node("RemoteTransform2D")


var ultima_direccion_introducida: = Vector2.ZERO
var velocidad: = Vector2.ZERO
var direccion_empuje: = Vector2.ZERO


func _physics_process(delta: float) -> void:
    if GameManager.on_batalla or GameDefinitions.on_gui: return 
    var direccion_entrada = _get_direccion_entrada()
    _mover(direccion_entrada,delta)
    _animar(direccion_entrada)

func _animar(direccion_entrada:Vector2) -> void:
    if direccion_entrada != Vector2.ZERO:
        animation_tree.set("parameters/Moverse/blend_position",direccion_entrada)
        animation_tree.set("parameters/Quieto/blend_position",direccion_entrada)
        animation_state.travel("Moverse")
    else:
        animation_state.travel("Quieto")


func _mover(direccion_entrada:Vector2,delta:float) -> void:
    ultima_direccion_introducida = direccion_entrada if direccion_entrada != Vector2.ZERO else ultima_direccion_introducida
    var dicc_valores = MoverSobreAireStrategy.ve(
        direccion_entrada,
        rapidez,
        rapidez_max,
        velocidad,
        delta
        )
    velocidad = dicc_valores.velocidad
    velocidad = move_and_slide(velocidad)
    velocidad += Vector2.UP.normalized()
    emit_signal("cambio_posicion",position)

func _get_direccion_entrada() -> Vector2:
    var direccion_entrada = Vector2.ZERO
    direccion_entrada.x = (
        Input.get_action_strength("ui_right") -
        Input.get_action_strength("ui_left")
    )
    direccion_entrada.y = (
        Input.get_action_strength("ui_down") -
        Input.get_action_strength("ui_up")
    )
    return direccion_entrada.normalized()

func asignar_remote_node(camara_path:NodePath) -> void:
    remote_node.remote_path = camara_path
    remote_node.use_global_coordinates = true

func desacoplar_remote_node() -> void:
    remote_node.remote_path = ""
    remote_node.use_global_coordinates = false