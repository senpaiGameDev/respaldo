const ACELERACION: = 1000
const DESACELERACION: = -2000
const DIRECCIONAMIENTO: = 40

static func ve(direccion:Vector2,rapidez:float,rapidez_max:float,velocidad:Vector2,delta:float)-> Dictionary:
    if direccion == Vector2.ZERO:
        rapidez += DESACELERACION * delta
    else:
        rapidez += ACELERACION * delta

    rapidez = clamp(rapidez,0,rapidez_max)
    var velocidad_objetivo: = rapidez * direccion
    var velocidad_de_direccionamiento = (velocidad_objetivo - velocidad).normalized() * DIRECCIONAMIENTO
    velocidad += velocidad_de_direccionamiento
    return {
       #llave = valor 
        velocidad = velocidad,
        rapidez = rapidez
    }