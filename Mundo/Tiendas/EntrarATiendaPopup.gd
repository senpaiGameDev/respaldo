extends ConfirmationDialog


func _ready() -> void:
	connect("popup_hide",self,"_on_popup_hide")

func _on_popup_hide() -> void:
	salir()

func entrar() -> void:
	GameDefinitions.on_gui = true
	grab_focus()
	popup_centered()

func salir() -> void:
	GameDefinitions.on_gui = false
	hide()

