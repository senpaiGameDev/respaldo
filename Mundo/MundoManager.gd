extends Node2D


onready var niveles_contenedor = get_node("Niveles")
onready var nave = get_node("Nave")
onready var canvas_layer = get_node("CanvasLayer")
onready var audio = get_node("Audio")
onready var camara_nave:Camera2D = get_node("CamaraNave")
onready var tiendas_areas = get_node("Tiendas")
onready var entrar_a_tienda_popup = get_node("CanvasLayer/EntrarATiendaPopup")

export(PackedScene) var pausa_scn_ref
export(PackedScene) var tienda_scn_ref

var nivel_actual:Nivel = null
var on_popup = false
var popup_actual = null

func _ready() -> void:
	entrar_a_tienda_popup.connect("confirmed",self,"_on_entrar_a_tienda_confirmed")
	for nivel in niveles_contenedor.get_children():
		nivel.connect("entro_nave",self,"_on_nivel_entro_nave")
		nivel.connect("salio_nave",self,"_on_nivel_salio_nave")
		nivel.connect("batalla_terminada",self,"_on_batalla_terminada")
		nivel.connect("batalla_iniciada", self,"_on_batalla_iniciada")
	for tienda_area in tiendas_areas.get_children():
		tienda_area.connect("area_entered",self,"_on_tienda_nave_entro")
		tienda_area.connect("area_exited",self,"_on_tienda_nave_salio")
		

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_select") and not GameManager.on_batalla:
		pausa()
	if on_popup:
		if event.is_action_pressed("ui_cancel"):
			on_popup = false
			if popup_actual != null:
				popup_actual.salir()

func pausa() -> void:
	var pausa_menu = pausa_scn_ref.instance()
	canvas_layer.add_child(pausa_menu)
	get_tree().paused = true

func _on_tienda_nave_entro(_area:Area2D) -> void:
	entrar_a_tienda_popup.entrar()

func _on_tienda_nave_salio(_area:Area2D) -> void:
	entrar_a_tienda_popup.salir()

func _on_nivel_entro_nave(nivel_gui) -> void:
	canvas_layer.add_child(nivel_gui)
	on_popup = true
	popup_actual = nivel_gui
	nivel_gui.entrar()

func _on_nivel_salio_nave(nivel_gui) -> void:
	on_popup = false
	nivel_gui.salir()
	canvas_layer.remove_child(nivel_gui)

func _on_entrar_a_tienda_confirmed() -> void:
	var tienda = tienda_scn_ref.instance()
	canvas_layer.add_child(tienda)
	get_tree().paused = true

func _on_batalla_terminada() -> void:
	camara_nave.make_current()
	audio.play()
	yield(SceneManager,"fade_out_completed")
	GameManager.on_batalla = false
	GameDefinitions.on_gui = false

func _on_batalla_iniciada() -> void:
	salir_mundo()
	GameManager.on_batalla = true

func salir_mundo() -> void:
	audio.stop()

