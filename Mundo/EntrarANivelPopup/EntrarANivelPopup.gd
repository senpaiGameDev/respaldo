extends PopupPanel

const sub_nivel_ui_ref = preload("res://Mundo/EntrarANivelPopup/sub_level_ui/sub_level_ui.tscn") 

var sub_niveles_contenedor 

class Estructura_sub_nivel:
	var sub_nivel_ui
	var sub_nivel_data

	func _init(sub_nivel_ui_n,sub_nivel_data_n):
		self.sub_nivel_ui = sub_nivel_ui_n
		self.sub_nivel_data = sub_nivel_data_n

	func lanzar_batalla() -> void:
		sub_nivel_data.lanzar_batalla()

	func set_ui_completado(valor) -> void:
		self.sub_nivel_ui.set_completado(valor)
	
var sub_niveles = []

func entrar() -> void:
	GameDefinitions.on_gui = true
	set_as_minsize()
	popup_centered()

func salir() -> void:
	GameDefinitions.on_gui = false
	hide()

func iniciar(sub_niveles_data) -> void:
	sub_niveles_contenedor = get_node("sub_niveles")
	for data in sub_niveles_data:
		var sub_nivel_ui:TextureButton =  sub_nivel_ui_ref.instance()
		sub_nivel_ui.iniciar(data.fondo,data.nivel_raiz,data.sub_nivel_nombre,data.oleadas)

		var estructura = Estructura_sub_nivel.new(sub_nivel_ui,data)
		sub_niveles.append(estructura)

		var sub_nivel_index = sub_niveles.size() - 1
		sub_niveles_contenedor.add_child(sub_nivel_ui)

		if data.id in GameManager.partida.get_niveles_completados():
			sub_nivel_ui.set_completado(true)

		sub_nivel_ui.connect("button_up",self,"_on_sub_nivel_ui_click",[sub_nivel_index])


func _on_sub_nivel_ui_click(sub_nivel_index:int) -> void:
	salir()
	sub_niveles[sub_nivel_index].lanzar_batalla()

func set_sub_nivel_ui_completado_por_id(sub_nivel_id:int) -> void:
	for sub_nivel in sub_niveles:
		if sub_nivel.sub_nivel_data.id == sub_nivel_id:
			sub_nivel.set_ui_completado(true)
