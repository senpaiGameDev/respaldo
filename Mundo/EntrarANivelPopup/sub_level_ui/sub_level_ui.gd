extends TextureButton


onready var icono_nodo:TextureRect = get_node("Container/Icono")
onready var nivel_titulo_nodo:Label = get_node("Container/Titulos/Nivel")
onready var sub_nivel_nombre_nodo:Label = get_node("Container/Titulos/Sub Nivel")
onready var oleadas_nodo:Label = get_node("Container/Info/Oleadas")
onready var completado_nodo:Label = get_node("Container/Info/Compleado")

var icono
var nivel_titulo
var sub_nivel_nombre
var oleadas

var completado = false
var inicializado =  false
var cargardo_en_memoria = false

func iniciar(icono_n:Texture,nivel:String,sub_nivel:String, oleadas_n:int) -> void:
	icono = icono_n
	nivel_titulo = nivel
	sub_nivel_nombre = sub_nivel
	oleadas = str(oleadas_n) + " Oleadas" if oleadas_n > 1 else str(oleadas_n) + " Oleada"
	inicializado = true

func set_completado(valor: bool) -> void:
	completado = valor
	if cargardo_en_memoria:
		completado_nodo.text = "Completado" if completado else ""
		modulate = Color.greenyellow if completado else Color.white

func _ready() -> void:
	cargardo_en_memoria = true
	if inicializado:
		icono_nodo.texture = icono
		nivel_titulo_nodo.text = nivel_titulo
		sub_nivel_nombre_nodo.text = sub_nivel_nombre
		oleadas_nodo.text = oleadas
		completado_nodo.text = "Completado" if completado else "Incompleto"
		modulate = Color.greenyellow if completado else Color.white
