class_name Articulo
extends Node

signal proceso_de_articulo_terminado

export(PackedScene) var animacion_ref = null

var id:int
var nombre:String
var cantidad:int
var objetivo:Actor = null

var animacion:Animacion_articulo = null

func set_atributos(id:int, nombre:String, cantidad:int) -> void:
	self.id = id
	self.nombre = nombre
	self.cantidad = cantidad

func utilizar(objetivo:Actor) -> void:
	self.objetivo = objetivo
	_instanciar_animacion()
	animacion.correr_animacion()
	yield(animacion,"animacion_terminada")

	var timer = get_tree().create_timer(animacion.TIEMPO_ANIMACION)
	timer.connect("timeout", self, "_on_terminar")

func _instanciar_animacion() -> void:
	animacion = animacion_ref.instance()
	add_child(animacion)
	animacion.global_position = objetivo.global_position

func _on_terminar() -> void:
	_aplicar_efecto()
	if is_instance_valid(animacion):
		animacion.queue_free()
	GameManager.partida.inventario.quitar_articulo(id,1)
	emit_signal("proceso_de_articulo_terminado")

func _aplicar_efecto() -> void:
	pass

