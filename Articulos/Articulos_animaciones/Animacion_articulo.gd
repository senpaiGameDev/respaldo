class_name Animacion_articulo
extends Node2D

signal animacion_terminada

export(float) var TIEMPO_ANIMACION = 0.8

onready var particulas:Particles2D = get_node("Particulas")
onready var efecto_de_sonido_articulo:AudioStreamPlayer2D = get_node("Audio")


func _ready() -> void:
	particulas.emitting = false
	visible = false
	get_tree().create_timer(0.5).connect("timeout",self,"correr_animacion")

func correr_animacion() -> void:
	get_tree().create_timer(TIEMPO_ANIMACION).connect("timeout", self,"deneter_particulas")
	visible = true
	particulas.emitting = true
	efecto_de_sonido_articulo.play()

func deneter_particulas() -> void:
	particulas.emitting = false
	emit_signal("animacion_terminada")



