extends Articulo

func _aplicar_efecto() -> void:
    if is_instance_valid(objetivo):
        objetivo.cambiar_poder_magico(cantidad * -1)

