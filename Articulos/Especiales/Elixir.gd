extends Articulo

func _aplicar_efecto() -> void:
    if is_instance_valid(objetivo):
        objetivo.recibir_agravio(objetivo.vida_max * -1)
        objetivo.cambiar_poder_magico(objetivo.mp_max *-1)


