extends Object

const INV_PATH = "user://inventario_%0d.save"
const name = "Inventario"

const articulos_key = "articulos"
const armas_key = "armas"
const monedas_key = "monedas"

signal cambio_inventario_articulos(id_articulo,nueva_cantidad)
signal cambio_inventario_armas(id_personaje,armas_disponibles)
signal cambio_inventario_monedas(monedas)

enum PERSONAJES_IDs {CABALLERO = 0, GUERRERO = 1, MAGA_BLANCA = 2, MAGA_OSCURA = 3, NINJA = 4}
enum ARMAS_MATERIALES { HIERRO,DIAMANTE,MADERA,OBSIDIANA,DEMONIUM }

var inventario = {}
var inventario_default  = {
	articulos_key:{
		ItemDefinitions.D_Hoja_de_Fenix.id: 1,
		ItemDefinitions.D_PosionCH_HP.id: 2,
		ItemDefinitions.D_PosionCH_MP.id: 1,
		ItemDefinitions.D_Elixir.id: 1
	},
	armas_key:{  #como cada personaje tiene una arma especifica ocuparemos los id de los jugdores
		PERSONAJES_IDs.CABALLERO: [ARMAS_MATERIALES.MADERA,ARMAS_MATERIALES.HIERRO],        
		PERSONAJES_IDs.GUERRERO: [ARMAS_MATERIALES.MADERA,ARMAS_MATERIALES.HIERRO],        
		PERSONAJES_IDs.MAGA_BLANCA: [ARMAS_MATERIALES.MADERA,ARMAS_MATERIALES.HIERRO],        
		PERSONAJES_IDs.MAGA_OSCURA: [ARMAS_MATERIALES.MADERA,ARMAS_MATERIALES.HIERRO],        
		PERSONAJES_IDs.NINJA: [ARMAS_MATERIALES.MADERA,ARMAS_MATERIALES.HIERRO]        
	},
	monedas_key: 10
}

func _init() -> void:
	inventario = inventario_default

func cargar(slot_id:int) -> void:
	var path = INV_PATH % slot_id
	var inventario_guardado = File.new()
	if inventario_guardado.file_exists(path):
		var respuesta = inventario_guardado.open(path,File.READ)

		if respuesta == OK:
			var data = parse_json(inventario_guardado.get_as_text())
			if typeof(data) == TYPE_DICTIONARY:
				_asignar_data(data)
			else:
				inventario_guardado.close()
				Debugger.log_error("Archivo de inventario corrupto",name)
		else:
			inventario_guardado.close()
			Debugger.log_error("Archivo de inventario corrupto", name)
		inventario_guardado.close()
	else:
		Debugger.log("inventario no guardado", name)

func _asignar_data(data) -> void:
	for key in data[articulos_key]:
		inventario[articulos_key][int(key)] = data[articulos_key][key]
	for key in data[armas_key]:
		inventario[armas_key][int(key)] = data[armas_key][key]
	inventario[monedas_key] = data[monedas_key]
	Debugger.log("inventario cargado : %s" % str(inventario),name)

func guardar(slot_id:int) -> void:
	var path = INV_PATH % slot_id
	var inventario_guardado = File.new()
	var respuesta = inventario_guardado.open(path,File.WRITE)
	if respuesta == OK:
		inventario_guardado.store_string(to_json(inventario))
	else:
		Debugger.log_error("No fue posible guardar el inventario", name)

	inventario_guardado.close()

func obtener_cantidad_articulo(id:int) -> int:
	if inventario[articulos_key].has(id):
		return inventario[articulos_key][id]
	return -1

func set_tiene_arma(personaje_id: int, arma_material) -> bool:
	return arma_material in inventario[armas_key][personaje_id]

func agregar_articulo(id: int, cantidad: int)-> void:
	if ItemDefinitions.existe_articulo(id):
		if inventario[articulos_key].has(id):
			inventario[articulos_key][id] += cantidad
		else:
			inventario[articulos_key][id] = cantidad
		emit_signal("cambio_inventario_articulos",id,inventario[articulos_key][id])

func quitar_articulo(id:int, cantidad:int) -> void:
	if inventario[articulos_key].has(id):
		inventario[articulos_key][id] -= cantidad
		Debugger.log("Se quito " + str(cantidad) +" aritculos(s) id: "  + str(id), name)
		Debugger.log(str(inventario), name)
		emit_signal("cambio_inventario_articulos",id,inventario[articulos_key][id])

func agregar_arma(jugador_id:int, arma_frame:int) -> void:
	assert(jugador_id in PERSONAJES_IDs.values(),"[%s]:: jugador_id: %d no existe" % [name,jugador_id])
	assert(arma_frame in ARMAS_MATERIALES.values(),"[%s]:: arma_frame: %d no existe" % [name, arma_frame])
	if inventario[armas_key].has(jugador_id):
		inventario[armas_key][jugador_id].append(arma_frame)
		emit_signal("cambio_inventario_armas",jugador_id,inventario[armas_key][jugador_id])


func quitar_arma(jugador_id:int, arma_frame:int) -> void:
	assert(jugador_id in PERSONAJES_IDs.values(),"[%s]:: jugador_id: %d no existe" % [name,jugador_id])
	assert(arma_frame in ARMAS_MATERIALES.values(),"[%s]:: arma_frame: %d no existe" % [name, arma_frame])
	if inventario[armas_key].has(jugador_id):
		inventario[armas_key][jugador_id].erase(arma_frame)
		emit_signal("cambio_inventario_armas",jugador_id,inventario[armas_key][jugador_id])

func agregar_monedas(cantidad:int) -> void:
	inventario[monedas_key] += cantidad
	emit_signal("cambio_inventario_monedas",inventario[monedas_key])

func quitar_monedas(cantidad:int) -> bool:
	if cantidad > inventario[monedas_key]:
		return false	
	else:
		inventario[monedas_key] -= cantidad
		emit_signal("cambio_inventario_monedas",inventario[monedas_key])
		return true


func get_armas_de_jugador(jugador_id:int) -> Array:
	assert(jugador_id in PERSONAJES_IDs.values(),"[%s]:: jugador_id: %d no existe" % [name,jugador_id])
	if inventario[armas_key].has(jugador_id):
		return inventario[armas_key][jugador_id]
	else:
		return []

func se_tiene_articulo(id) -> bool:
	if inventario[articulos_key].has(id):
		if inventario[articulos_key][id] > 0:
			return true
	return false

func get_articulos() -> Dictionary:
	return inventario[articulos_key]

func get_armas() -> Dictionary:
	return inventario[armas_key]

func get_monedas() -> int:
	return inventario[monedas_key]
