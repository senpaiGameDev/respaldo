extends Object

var hp_max
var mp_max 
var fuerza
var poder_magico 
var resistencia
var resistencia_magica 
var experiencia 
var nivel 
var tiempo_espera 
var exp_siguiente_nivel

func _init(hp,mp,fuerza_n,poder_m,resistencia_f,resistencia_m,experiencia_n,nivel_n,tiempo) -> void:
	hp_max = hp
	mp_max = mp 
	fuerza = fuerza_n
	poder_magico = poder_m
	resistencia = resistencia_f
	resistencia_magica = resistencia_m
	experiencia = experiencia_n
	nivel = nivel_n
	tiempo_espera = tiempo
	exp_siguiente_nivel = self.siguiente_nivel(nivel)

func siguiente_nivel(nivel_n) -> int:
	return int(round(( 4 * ((nivel_n + 1) ^ 3)) / 5)) 

func get_data() -> Dictionary:
	return {
		"hp_max": hp_max,
		"mp_max": mp_max,
		"fuerza": fuerza,
		"poder_magico": poder_magico,
		"resistencia": resistencia,
		"resistencia_magica": resistencia_magica,
		"experiencia": experiencia,
		"nivel": nivel,
		"tiempo_espera": tiempo_espera
	}

