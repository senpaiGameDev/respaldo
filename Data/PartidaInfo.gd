extends Object

var vacio = true
var sub_nivel_id: int
var grupo_activo = []
var tiempo = 0

func _init(vacio_n,id,grupo_activo_n,tiempo_n) -> void:
	vacio = vacio_n
	sub_nivel_id = id
	grupo_activo = grupo_activo_n.duplicate()
	tiempo = tiempo_n

func get_data() -> Dictionary:
	return {
		"vacio": vacio,
		"sub_nivel_id": sub_nivel_id,
		"grupo_activo": grupo_activo,
		"tiempo": tiempo
	}