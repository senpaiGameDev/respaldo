extends Object

const save_path = "user://ConfigJugadores_%0d.save"
const name = "ConfiguracionJugadores"
const stats = "stats"
const arma_frame = "arma_frame"

var estadisticas = preload("res://Data/EstadisticasRaw.gd")

var caballero_def_stats = estadisticas.new(
	180, #hp 
	30,  #mp
	40,  #fuerza
	25,  #poder magico 
	15,  #resistencia 
	15,  #resistencia magica
	1,   #exp
	1,   #nivel
	2.5  #tiempo de espera
)


var guerrero_def_stats = estadisticas.new(
	200, #hp 
	10,  #mp
	40,  #fuerza
	5,  #poder magico 
	20,  #resistencia 
	20,  #resistencia magica
	1,   #exp
	1,   #nivel
	4  #tiempo de espera
)

var maga_blanca_def_stats = estadisticas.new(
	100, #hp 
	50,  #mp
	5,  #fuerza
	45,  #poder magico 
	5,  #resistencia 
	10,  #resistencia magica
	1,   #exp
	1,   #nivel
	3  #tiempo de espera
)

var maga_oscura_def_stats = estadisticas.new(
	150, #hp 
	40,  #mp
	10,  #fuerza
	40,  #poder magico 
	10,  #resistencia 
	15,  #resistencia magica
	1,   #exp
	1,   #nivel
	3  #tiempo de espera
)

var ninja_def_stats = estadisticas.new(
	100, #hp 
	20,  #mp
	50,  #fuerza
	30,  #poder magico 
	7,  #resistencia 
	5,  #resistencia magica
	1,   #exp
	1,   #nivel
	2  #tiempo de espera
)

enum PERSONAJES_IDs {CABALLERO = 0, GUERRERO = 1, MAGA_BLANCA = 2, MAGA_OSCURA = 3, NINJA = 4}
enum ARMAS_MATERIALES { HIERRO,DIAMANTE,MADERA,OBSIDIANA,DEMONIUM }

var jugadores_configs = {
	PERSONAJES_IDs.CABALLERO:{
		stats: caballero_def_stats.get_data(),
		arma_frame: ARMAS_MATERIALES.MADERA 
	},
	PERSONAJES_IDs.GUERRERO:{
		stats: guerrero_def_stats.get_data(),
		arma_frame: ARMAS_MATERIALES.MADERA
	},
	PERSONAJES_IDs.MAGA_BLANCA:{
		stats: maga_blanca_def_stats.get_data(),
		arma_frame: ARMAS_MATERIALES.MADERA
	},
	PERSONAJES_IDs.MAGA_OSCURA:{
		stats: maga_oscura_def_stats.get_data(),
		arma_frame: ARMAS_MATERIALES.MADERA
	},
	PERSONAJES_IDs.NINJA:
	{
		stats: ninja_def_stats.get_data(),
		arma_frame: ARMAS_MATERIALES.MADERA
	}
}

func guardar(slot_id:int) -> void:
	var path = save_path % slot_id
	var file = File.new()

	var error = file.open(path, File.WRITE)

	if error == OK:
		var json = to_json(jugadores_configs)
		file.store_line(json)
	else:
		file.close()
		Debugger.log_error("No se puede guardar el archivo:  %s" % path, name)

	file.close()
	

func cargar(slot_id:int) -> void:
	var path = save_path % slot_id
	var file = File.new()

	if file.file_exists(path):
		var error = file.open(path,File.READ)
		if error == OK:

			var data = parse_json(file.get_as_text())
			if typeof(data) == TYPE_DICTIONARY:
				_asignar_data(data)
			else:
				Debugger.log_error("Error al cargar la configuración: %s " % path, name)
		else:
			file.close()
			Debugger.log_error("Archivo %s corrupto, no se pudo cargar la configuración" % path, name)

		file.close()
	else:
		Debugger.log_error("El archivo que intentas cargar no exite", name)

func _asignar_data(data) -> void:
	for key in data.keys():
		jugadores_configs[int(key)] = data[key]
	Debugger.log("configuración cargada: %s" % str(jugadores_configs), name)


func get_stats_jugador(jugador_id:int) -> Dictionary:
	assert(jugador_id in PERSONAJES_IDs.values(),"[%s]:: jugador_id no existe" % name)
	assert(jugadores_configs.has(jugador_id),"[%s]::jugador_id incorrecto" % name)
	return jugadores_configs[jugador_id][stats]

func get_arma_jugador(jugador_id:int) -> int:
	assert(jugador_id in PERSONAJES_IDs.values(),"[%s]:: jugador_id: %d no existe" % [name,jugador_id])
	return jugadores_configs[jugador_id][arma_frame]

func actualizar_exp_jugador(jugador_id:int, exp_n:int) -> void:
	assert(jugador_id in PERSONAJES_IDs.values(),"[%s]:: jugador_id no existe" % name)
	assert(jugadores_configs.has(jugador_id),"[%s]::jugador_id incorrecto" % name)
	jugadores_configs[jugador_id][stats]["experiencia"] = exp_n
	Debugger.log("Experiencia de jugador %s Actualizada, Data: %s" %[jugador_id,str(jugadores_configs[jugador_id])],name)


func actualizar_stats_jugador(jugador_id: int, stats_n:Dictionary) -> void:
	assert(jugador_id in PERSONAJES_IDs.values(),"[%s]:: jugador_id no existe" % name)
	assert(jugadores_configs.has(jugador_id),"[%s]::jugador_id incorrecto" % name)
	jugadores_configs[jugador_id][stats] = stats_n
	Debugger.log("stats de jugador %s Actualizados, Data: %s" %[jugador_id,str(jugadores_configs[jugador_id])],name)

func actualizar_arma_jugador(jugador_id: int, arma_frame_n:int) -> void:
	assert(jugador_id in PERSONAJES_IDs.values(),"[%s]:: jugador_id: %d no existe" % [name,jugador_id])
	assert(arma_frame_n in ARMAS_MATERIALES.values(),"[%s]:: arma_frame: %d no existe" % [name, arma_frame_n])
	jugadores_configs[jugador_id][arma_frame] = arma_frame_n
	Debugger.log("Arma de jugador %s Actualizada, Data: %s" % [jugador_id, str(jugadores_configs[jugador_id])],name)
