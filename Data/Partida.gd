extends Object

signal cambio_grupo_activo(nuevo_grupo)
signal cambio_niveles_completados(niveles_completados)

const PARTIDA_PATH = "user://partida_%01d.save"
const inventario_ref = preload("res://Data/Inventario.gd")
const configuracion_jugadores_ref = preload("res://Data/ConfiguracionJugadores.gd")
const grupo_activo_key = "grupo_activo"
const niveles_completados_key = "niveles_completados"

const name = "Partida"


var data = {
	grupo_activo_key: [
		GameDefinitions.Caballero_escena,
		GameDefinitions.Maga_blanca_escena,
		GameDefinitions.Maga_oscura_escena
		],
	niveles_completados_key: [],
} 

var inventario
var configuracion_jugadores

func _init() -> void:
	inventario = inventario_ref.new()
	configuracion_jugadores = configuracion_jugadores_ref.new()

func guardar_partida(slot_id:int):
	var save_path = PARTIDA_PATH % slot_id
	var file = File.new()
	var error = file.open(save_path,File.WRITE)
	if error == OK:
		file.store_line(to_json(data))
		file.close()

		inventario.guardar(slot_id)
		configuracion_jugadores.guardar(slot_id)	
	else:
		Debugger.log_error("La partida %s no se pudo guardar, error: %d" % [save_path,error], name)
		file.close()

func cargar_partida(slot_id:int):
	var load_path = PARTIDA_PATH % slot_id
	var file = File.new()
	if file.file_exists(load_path):
		var error = file.open(load_path,File.READ)
		if error == OK:
			var json_data = parse_json(file.get_as_text())

			if typeof(json_data) != TYPE_DICTIONARY:
				file.close()
				Debugger.log_error("datos de archivo de partida %s corruptos", name)
				return

			data = json_data
			inventario.cargar(slot_id)
			configuracion_jugadores.cargar(slot_id)
			file.close()
		else:
			file.close()
			Debugger.log_error("Archivo de partida % corruto",name)

	else:
		Debugger.log_error("El Archivo de partida %s no existe" % load_path ,name)

func get_grupo_activo() -> Array:
	return data[grupo_activo_key]

func get_niveles_completados() -> Array:
	return data[niveles_completados_key]

func cambiar_grupo(nueva_escena:String,quitar_escena:String) -> void:
	assert(nueva_escena in GameDefinitions.lista_jugadores, 
		"[Partida]:: el nuevo integrante: %s no existe en GameDefinitions" % nueva_escena)
		
	var index = data[grupo_activo_key].find(quitar_escena)
	if index != -1:
		data[grupo_activo_key].remove(index)
		data[grupo_activo_key].append(nueva_escena)
		emit_signal("cambio_grupo_activo",data[grupo_activo_key])
	else:
		Debugger.log("No se encontro al jugador : %s " % quitar_escena, name)

func sub_nivel_completado(nivel_id:int)-> bool:
	if nivel_id in data[niveles_completados_key]: return false
	data[niveles_completados_key].append(nivel_id)
	emit_signal("cambio_niveles_completados", data[niveles_completados_key])
	return true
